﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webstore.Domain.Entities;

namespace WebStore.Models
{
    public class CategoriesViewModel
    {
        public List<CategoryViewModel> Categories { get; protected set; }

        //public CategoriesViewModel(IEnumerable<Category> categories)
        //{

        //    Categories = new List<CategoryViewModel>();
        //    foreach (Category category in categories)
        //    {
        //        this.Categories.Add(new CategoryViewModel(category));
        //    }

        //}
    }
}