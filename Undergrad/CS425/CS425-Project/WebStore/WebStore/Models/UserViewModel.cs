﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Webstore.Domain.Entities;
using Webstore.Domain.HashAndSalt;

namespace WebStore.Models
{
    public class UserViewModel
    {
        static PasswordManager pwdManager = new PasswordManager();
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter your first name")]
        [RegularExpression(@"([a-zA-Z0-9 .&'-]+)", ErrorMessage = "Enter only letters and numbers for First Name")]
        public String FName { get; set; }

        [Required(ErrorMessage = "Please enter your last name")]
        [RegularExpression(@"([a-zA-Z0-9 .&'-]+)", ErrorMessage = "Enter only letters and numbers for Last Name")]
        public String LName { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        [RegularExpression(@".+\@.+\..+", ErrorMessage = "Enter a valid email address")]
        public String Email { get; set; }

        [Required(ErrorMessage = "Please enter your password")]
        [RegularExpression(@"([a-zA-Z0-9 .&'-]+)", ErrorMessage = "Retry. Characters allowed: spaces a-z A-Z . & ' or -")]
        public String Password { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }

        public int Permission { get; set; }

        public void GeneratePasswordHash()
        {
            string salt = null;
            PasswordHash = pwdManager.GeneratePasswordHash(Password, out salt);
            this.Salt = salt;
        }

        public User User
        {
            get
            {
                User tempUser = new User() { UserId = this.Id, FName = this.FName, LName = this.LName, Email = this.Email, PasswordHash = this.PasswordHash, Salt = this.Salt, Permission = this.Permission};
                return tempUser;
            }
        }

        public UserViewModel(User user)
        {
            Id = user.UserId;
            FName = user.FName;
            LName = user.LName;
            Email = user.Email;
            PasswordHash = user.PasswordHash;
            Salt = user.Salt;
            Permission = user.Permission;
        }

        public UserViewModel()
        {
            
        }
    }
}