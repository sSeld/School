﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webstore.Domain.Concrete;
using Webstore.Domain.Entities;

namespace WebStore.Models
{
    public class ProductViewModel
    {
        public Product Product
        {
            get
            {
                Product tempProduct = new Product
                {
                    ProductId = this.Id,
                    Name = this.Name,
                    Price = this.Price,
                    Inventory = this.Inventory,
                    ImageUrl = this.ImageUrl,
                    Description = this.Description
                };
                List<CategoryProduct> tempCategoryProducts = new List<CategoryProduct>();
                if (AllCategories != null)
                {
                    for (int i = 0; i < AllCategories.Count; i++)
                    {
                        if (SelectedCategories[i])
                            tempCategoryProducts.Add(new CategoryProduct
                            {
                                CategoryId = AllCategories.ElementAt(i).Id,
                                ProductId = this.Id
                            });
                    }
                }
                
                tempProduct.CategoriesProducts = tempCategoryProducts;
                return tempProduct;
            }
        }


        // public IEnumerable<bool> SelectedCategories {get; set;}

        public int Id { get; set; }
        public String Name { get; set; }
        public float Price { get; set; }
        public int Inventory { get; set; }
        public String ImageUrl { get; set; }
        public string Description { get; set; }

        public List<CategoryViewModel> Categories { get; set; }
        public ProductViewModel(int id, String name, float price, int inventory, string photo)
        {
            Categories = new List<CategoryViewModel>();
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.Inventory = inventory;
            this.ImageUrl = photo;
            this.Description = Description;
        }

        public ProductViewModel(Product product)
        {
            Categories = new List<CategoryViewModel>();
            this.Id = product.ProductId;
            this.Name = product.Name;
            this.Price = product.Price;
            this.Inventory = product.Inventory;
            this.ImageUrl = product.ImageUrl;
            this.Description = product.Description;
        }

        public List<CategoryViewModel> AllCategories { get; set; }
        public bool[] SelectedCategories { get; set; }

        public ProductViewModel(Product product, IEnumerable<Category> categories)
            : this(product)
        {
            var result = from c in categories
                         join cp in product.CategoriesProducts on c.CategoryId equals cp.CategoryId
                         where (cp.ProductId == product.ProductId)
                         select new
                         {
                             CategoryId = c.CategoryId,
                             Name = c.Name
                         };
            foreach (var c in result)
            {
                Categories.Add(new CategoryViewModel(c.CategoryId, c.Name));
            }
            AllCategories = new List<CategoryViewModel>();
            this.SelectedCategories = new bool[categories.Count()];
            foreach (Category tempCategory in categories)
            {
                AllCategories.Add(new CategoryViewModel(tempCategory));
                bool tempBool = false;
                foreach (CategoryProduct tempCategoryProduct in product.CategoriesProducts)
                {
                    if (tempCategoryProduct.CategoryId == tempCategory.CategoryId)
                    {
                        tempBool = true;
                    }
                }
                SelectedCategories[AllCategories.Count - 1] = tempBool;
            }
        }

        public ProductViewModel()
        {
            EFStoreRepository repository = new EFStoreRepository();
            AllCategories = new List<CategoryViewModel>();
            foreach (Category tempCategory in repository.Categories)
            {
                AllCategories.Add(new CategoryViewModel(tempCategory));
            }
        }

        override public String ToString()
        {
            return this.Name;
        }
    }
}