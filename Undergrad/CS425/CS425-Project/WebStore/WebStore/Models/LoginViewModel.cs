﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebStore.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please enter your email")]
        [RegularExpression(@".+\@.+\..+", ErrorMessage = "Enter a valid email address")]
        public String Email { get; set; }

        [Required(ErrorMessage = "Please enter your password")]
        [RegularExpression(@"([a-zA-Z0-9 .&'-]+)", ErrorMessage = "Retry. Characters allowed: spaces a-z A-Z . & ' or -")]
        public String Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}