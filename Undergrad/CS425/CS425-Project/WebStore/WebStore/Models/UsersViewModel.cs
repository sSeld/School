﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebStore.Models
{
    public class UsersViewModel
    {
        public List<UserViewModel> UserViewModels { get; set; }
        public UsersViewModel()
        {
            UserViewModels = new List<UserViewModel>();
        }
    }
}