﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webstore.Domain.Entities;

namespace WebStore.Models
{
    public class CartIndexViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}