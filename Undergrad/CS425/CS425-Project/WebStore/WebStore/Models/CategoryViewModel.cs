﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webstore.Domain.Concrete;
using Webstore.Domain.Entities;

namespace WebStore.Models
{
    public class CategoryViewModel
    {
        public Category Category
        {
            get
            {
                Category tempCategory = new Category
                {
                    Name = this.Name,
                };
                return tempCategory;
            }
        }

        //private Category category;
        private IEnumerable<Product> enumerable;

        public int Id { get; protected set; }
        public String Name { get; set; }

        public List<ProductViewModel> Products { get; protected set; }

        public CategoryViewModel(int id, String name)
        {
            this.Id = id;
            Products = new List<ProductViewModel>();
            this.Name = name;
        }

        public CategoryViewModel(Category category)
        {
            Id = category.CategoryId;
            Name = category.Name;

            Products = new List<ProductViewModel>();
        }

        public CategoryViewModel(Category category, IEnumerable<Product> products)
            : this(category)
        {

            var result = from p in products
                         join cp in category.CategoriesProducts on p.ProductId equals cp.ProductId
                         where (cp.CategoryId == category.CategoryId) //only necessary if products contains ALL the products
                         select new
                         {
                             ProductId = p.ProductId,
                             Name = p.Name,
                             Price = p.Price,
                             Inventory = p.Inventory,
                             ImageUrl = p.ImageUrl,
                             Description = p.Description,
                         };
            foreach (var p in result)
            {
                Products.Add(new ProductViewModel(p.ProductId, p.Name, p.Price, p.Inventory, p.ImageUrl));
            }
        }

        public CategoryViewModel()
        {
            
        }
        override public String ToString()
        {
            return Name;
        }
    }
    
}