﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webstore.Domain.Entities;

namespace WebStore.Models
{
    public class ListViewModel
    {
        public List<CategoryViewModel> Categories { get; protected set; }
        public List<ProductViewModel> Products { get; protected set; }

        public ListViewModel(IEnumerable<Category> categories)
        {

            Categories = new List<CategoryViewModel>();
            foreach (Category category in categories)
            {
                this.Categories.Add(new CategoryViewModel(category));
            }

        }

        public ListViewModel(IEnumerable<Product> products)
        {
            Products = new List<ProductViewModel>();
            foreach (Product product in products)
            {
                this.Products.Add(new ProductViewModel(product));
            }
        }

        public ListViewModel(IEnumerable<Category> categories, IEnumerable<Product> products)
        {
            Categories = new List<CategoryViewModel>();
            Products = new List<ProductViewModel>();
            foreach (Category category in categories)
            {
                this.Categories.Add(new CategoryViewModel(category));
            }
            
            foreach (Product product in products)
            {
                this.Products.Add(new ProductViewModel(product));
            }
        }
    }
}