﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webstore.Domain.Concrete;
using Webstore.Domain.Entities;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class AdminController : Controller
    {
        // GET: /Admin/
        public ActionResult AdminIndex()
        {
            if (CheckPermission())
            {
                EFStoreRepository repository = new EFStoreRepository();
                ListViewModel categoriesProducts = new ListViewModel(repository.Categories, repository.Products);
                return View(categoriesProducts);
            }
            return View("AccessDenied");
        }

        ////////////////////////////////////////////////////////////////////////////////////
        // Products ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        
        public ActionResult Products()
        {
            if (CheckPermission())
            {
                EFStoreRepository repository = new EFStoreRepository();

                ListViewModel categoriesProducts = new ListViewModel(repository.Categories, repository.Products);
                return View(categoriesProducts);
            }
            return View("AccessDenied");
        }

        public ActionResult DeleteProduct(int productID)
        {
            EFStoreRepository repository = new EFStoreRepository();

            Product product = repository.Products.Single(p => p.ProductId == productID);
            repository.RemoveProduct(product);
            return RedirectToAction("Products");
        }
        

        [HttpGet]
        public ActionResult EditProduct(int productID)
        {
            if (CheckPermission())
            {
                EFStoreRepository repository = new EFStoreRepository();
                Product product = repository.Products.Single(p => p.ProductId == productID);
                ProductViewModel productViewModel = new ProductViewModel(product, repository.Categories);

                return View(productViewModel);
            }
            return View("AccessDenied");
                    
            
        }

        [HttpPost]
        public RedirectToRouteResult EditProduct(ProductViewModel product)
        {
            if (CheckPermission())
            {
                EFStoreRepository repository = new EFStoreRepository();
                repository.UpdateProduct(product.Product);

                return RedirectToAction("EditProduct", new { productID = product.Id });
            }
            return RedirectToAction("AccessDenied");
                    
        }

        [HttpGet]
        public ActionResult AddProduct()
        {
            if (CheckPermission())
            {
                EFStoreRepository repository = new EFStoreRepository();
                ProductViewModel productViewModel = new ProductViewModel();
                return View(productViewModel);
            }
            return View("AccessDenied");
            
        }

        [HttpPost]
        public RedirectToRouteResult AddProduct(ProductViewModel productViewModel)
        {
            if (CheckPermission())
            {
                if (ModelState.IsValid)
                {// Valid
                    EFStoreRepository repository = new EFStoreRepository();
                    if (!repository.AddProduct(productViewModel.Product))
                    {// If User already exists
                        return RedirectToAction("Products");
                    }

                }
                else
                {// Invalid
                    return RedirectToAction("Products"); // Redirect to edit product? Not working
                }
            }
            return RedirectToAction("AccessDenied");
            
        }

        ////////////////////////////////////////////////////////////////////////////////////
        // Category ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////

        public ActionResult Categories()
        {
            if (CheckPermission())
            {
                EFStoreRepository repository = new EFStoreRepository();

                ListViewModel categoriesProducts = new ListViewModel(repository.Categories, repository.Products);
                return View(categoriesProducts);
            }
            return View("AccessDenied");
        }

        [HttpGet]
        public ActionResult AddCategory()
        {
            if (CheckPermission())
            {
                EFStoreRepository repository = new EFStoreRepository();
                
                return View();
            }
            return View("AccessDenied");
        }

        [HttpPost]
        public RedirectToRouteResult AddCategory(CategoryViewModel categoryViewModel)
        {
            if (CheckPermission())
            {
                if (ModelState.IsValid)
                {// Valid
                    EFStoreRepository repository = new EFStoreRepository();
                    if (!repository.AddCategory(categoryViewModel.Category))
                    {// If User already exists
                        return RedirectToAction("Categories");
                    }
                }
                else
                {// Invalid
                return RedirectToAction("Categories"); // Redirect to edit product? Not working
                }
            }
            return RedirectToAction("AccessDenied");

        }

        public bool CheckPermission()
        {
            if (GetUser() != null)
            {
                if (GetUser().Permission > 0)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        public UserViewModel GetUser()
        {

            return (UserViewModel)Session["User"];


        }
	}
}