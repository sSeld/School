﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webstore.Domain.Concrete;
using Webstore.Domain.Entities;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class CartController : Controller
    {

        public ViewResult CartIndex(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = GetCart(),
                ReturnUrl = returnUrl
            });
        }

        public RedirectToRouteResult AddToCart(int Id, string returnUrl)
        {
            EFStoreRepository repository = new EFStoreRepository();
            Product product = repository.Products
                .FirstOrDefault(p => p.ProductId == Id);

            if (product != null)
            {

                GetCart().AddItem(product, 1);

            }
            return RedirectToAction("CartIndex", new { returnUrl });
        }

        public RedirectToRouteResult RemoveFromCart(int productID, string returnUrl)
        {
            EFStoreRepository repository = new EFStoreRepository();
            Product product = repository.Products
                .FirstOrDefault(p => p.ProductId == productID);

            if (product != null)
            {
                GetCart().RemoveLine(product);
            }
            return RedirectToAction("CartIndex", new { returnUrl });
        }

        private Cart GetCart()
        {

            Cart cart = (Cart)Session["Cart"];
            if (cart == null)
            {
                cart = new Cart();
                Session["Cart"] = cart;
            }
            return cart;
        }

        public ViewResult Checkout()
        {
            EFStoreRepository repository = new EFStoreRepository();
            Cart cart = GetCart();
            UserViewModel user = GetCustomer();
            if (cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }
            if (user == null)
            {
                ModelState.AddModelError("", "Sorry, you must login!");
            }

            if (ModelState.IsValid)
            {
                string orderReport = cart.ProcessOrder(repository);
                cart.Clear();
                ViewBag.Report = orderReport;
                return View("Completed");
            }

            return View();

        }

        public UserViewModel GetCustomer()
        {

            return (UserViewModel)Session["User"];


        }
	}
}