﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webstore.Domain.Concrete;
using Webstore.Domain.Entities;
using Webstore.Domain.HashAndSalt;
using WebStore.Models;

namespace WebStore.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            EFStoreRepository repository = new EFStoreRepository();
            ListViewModel products = new ListViewModel(repository.Products);
            return View(products);
        }

        [HttpGet]
        public ActionResult Register()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Register(UserViewModel userViewModel)
        {
            
            if (ModelState.IsValid)
            {// Valid
                EFStoreRepository repository = new EFStoreRepository();
                userViewModel.GeneratePasswordHash();
                if (!repository.AddUser(userViewModel.User))
                {// If User already exists
                    return View();
                }
                User user = repository.Users.Single(c => c.Email == userViewModel.Email);
                
                    if ((userViewModel.Email == user.Email) && (userViewModel.PasswordHash == user.PasswordHash))
                    {
                        Session["User"] = new UserViewModel(user);
                        
                    }
               
               return View("RegisterComplete", userViewModel); 
            }
            else
            {// Invalid
                return View();
            }
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {// Valid
                PasswordManager pwdManager = new PasswordManager();
                EFStoreRepository repository = new EFStoreRepository();
                User user = repository.Users.Single(c => c.Email == loginViewModel.Email);
                bool result = pwdManager.IsPasswordMatch(loginViewModel.Password, user.Salt, user.PasswordHash);
                
                if (result)
                {
                    Session["User"] = new UserViewModel(user);
                    return View("LoginSuccessful", loginViewModel);
                }
                
                return View();
            }
            else
            {// Invalid
                return View();
            }
        }

        public ActionResult Books()
        {
            EFStoreRepository repository = new EFStoreRepository();
            Category category = new Category();
            
            foreach (Category tempCategory in repository.Categories)
            {
                if (tempCategory.CategoryId == 2) // 2 Represents Books
                { 
                    category = tempCategory;
                    break;
                }
            }
            CategoryViewModel categoryViewModel = new CategoryViewModel(category, repository.Products);
            return View(categoryViewModel);
        }

        public ActionResult Movies()
        {
            EFStoreRepository repository = new EFStoreRepository();
            Category category = new Category();

            foreach (Category tempCategory in repository.Categories)
            {
                if (tempCategory.CategoryId == 1) // 1 Represents Movies
                {
                    category = tempCategory;
                    break;
                }
            }
            CategoryViewModel categoryViewModel = new CategoryViewModel(category, repository.Products);
            return View(categoryViewModel);
        }

        public ActionResult About()
        {
            return View();
        }

        public ViewResult Product(int productID)
        {
            EFStoreRepository repository = new EFStoreRepository();
            Product product = repository.Products.Single(p => p.ProductId == productID);

            ProductViewModel productViewModel = new ProductViewModel(product, repository.Categories);
            return View(productViewModel);
        }


        public UserViewModel GetUser()
        {

            return (UserViewModel)Session["User"];


        }
    }
}