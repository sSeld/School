﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webstore.Domain.Entities;

namespace Webstore.Domain.Abstract
{
    public interface IStoreRepository
    {
        IEnumerable<Category> Categories { get; }
        IEnumerable<Product> Products { get; } 
        IEnumerable<User> Users { get; }
        IEnumerable<CategoryProduct> CategoriesProducts { get; } 
        bool AddUser(User user);
        void UpdateUser(User user);
        bool RemoveInventory(int productId, int quantity);
        bool AddProduct(Product product);
        bool RemoveProduct(Product product);
        bool AddCategory(Category category);
    }
}
