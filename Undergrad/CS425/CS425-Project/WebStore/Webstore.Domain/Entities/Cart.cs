﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webstore.Domain.Abstract;


namespace Webstore.Domain.Entities
{
    public class Cart
    {
        public List<CartLine> lineCollection = new List<CartLine>();

        public void AddItem(Product product, int quantity)
        {
            if (product.Inventory < quantity)
                return;
            CartLine line = lineCollection
                .Where(p => p.Product.ProductId == product.ProductId)
                .FirstOrDefault();

            if (line == null)
            {

                lineCollection.Add(new CartLine
                {
                    Product = product,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
                if (product.Inventory < line.Quantity)
                    line.Quantity = product.Inventory;
            }

        }
        public string ProcessOrder(IStoreRepository repository)
        {
            float sum = 0;
            string report = @"Customer Report<br /><br />";
            foreach (CartLine cartline in lineCollection)
            {

                if (repository.RemoveInventory(cartline.Product.ProductId, cartline.Quantity))
                {
                    sum += cartline.Product.Price * cartline.Quantity;
                    report += cartline.Product.Name + " " + "(" + cartline.Quantity + ")" + " * " + cartline.Product.Price + " = " + cartline.Quantity * cartline.Product.Price + @"<br />";
                }
            }
            report += @"_________________________________<br /> Total = " + sum + @"<br />";

            return report;

        }
        public void RemoveLine(Product product)
        {
            lineCollection.RemoveAll(l => l.Product.ProductId == product.ProductId);
        }

        public float ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Product.Price * e.Quantity);

        }
        public void Clear()
        {
            lineCollection.Clear();
        }

        public IEnumerable<CartLine> Lines
        {
            get { return lineCollection; }
        }




    }

    public class CartLine
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
