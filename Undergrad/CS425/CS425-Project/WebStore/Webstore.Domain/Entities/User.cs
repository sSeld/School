﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webstore.Domain.Entities
{
    public class User
    {
        public int UserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
        public int Permission { get; set; }

        internal void UpdateValues(User user)
        {
            this.Email = user.Email;
            this.FName = user.FName;
            this.LName = user.LName;
            this.PasswordHash = user.PasswordHash;
        }

        public User()
        {
            Permission = 0;
        }
    }
}
