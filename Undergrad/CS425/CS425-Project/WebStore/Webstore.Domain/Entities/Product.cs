﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webstore.Domain.Entities
{
    public class Product
    {
        public int ProductId { get; set; }
        public String Name { get; set; }
        public int Inventory { get; set; }
        public float Price { get; set; }
        public String ImageUrl { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CategoryProduct> CategoriesProducts { get; set; }
        public void RemoveCategories()
        {
            CategoriesProducts.Clear();
        }

        public void UpdateValues(Product product)
        {
            this.Name = product.Name;
            this.Price = product.Price;
            this.Inventory = product.Inventory;
            this.ImageUrl = product.ImageUrl;
            this.CategoriesProducts = product.CategoriesProducts;
        }

        public bool removeInventory(int n)
        {
            if ((Inventory - n) >= 0)
            {
                Inventory = Inventory - n;
                return true;
            }
            return false;
        }
    }
}
