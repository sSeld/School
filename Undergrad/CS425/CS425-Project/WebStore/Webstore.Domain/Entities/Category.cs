﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webstore.Domain.Entities
{
    public class Category
    {
        public int CategoryId { get; set; }
        public String Name { get; set; }

        public virtual ICollection<CategoryProduct> CategoriesProducts { get; set; } 
    }
}
