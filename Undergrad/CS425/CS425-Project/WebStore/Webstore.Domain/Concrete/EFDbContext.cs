﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webstore.Domain.Entities;

namespace Webstore.Domain.Concrete
{
    public class EFDbContext : DbContext 
    {
        public DbSet<Category> DbCategories { get; set; }
        public DbSet<Product> DbProducts { get; set; }
        public DbSet<User> DbUsers { get; set; }
        public DbSet<CategoryProduct> DbCategoriesProducts { get; set; }

        //Setting the relationship between Categories and Products
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CategoryProduct>().HasKey(c => new { c.CategoryId, c.ProductId });
            modelBuilder.Entity<Category>().HasMany(c => c.CategoriesProducts).WithRequired().HasForeignKey(c => c.CategoryId);
            modelBuilder.Entity<Product>().HasMany(c => c.CategoriesProducts).WithRequired().HasForeignKey(c => c.ProductId);
        }
    }
}
