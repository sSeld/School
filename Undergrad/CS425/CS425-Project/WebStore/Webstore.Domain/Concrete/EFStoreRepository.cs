﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webstore.Domain.Abstract;
using Webstore.Domain.Entities;

namespace Webstore.Domain.Concrete
{
    public class EFStoreRepository : IStoreRepository
    {
        private EFDbContext context = new EFDbContext();

        public IEnumerable<Category> Categories
        {
            get { return context.DbCategories; }
        }

        public IEnumerable<Product> Products
        {
            get { return context.DbProducts; }
        }
        public IEnumerable<User> Users
        {
            get { return context.DbUsers; }
        }

        public IEnumerable<CategoryProduct> CategoriesProducts
        {
            get { return context.DbCategoriesProducts; }
        }

        public void UpdateUser(User user)
        {

            User currentUser = Users.Single(c => c.UserId == user.UserId);
            currentUser.UpdateValues(user);
            context.SaveChanges();
        }

        public bool AddUser(User user)
        {
            foreach (User tempUser in Users)
            {
                if (tempUser.Email.CompareTo(user.Email) == 0)
                    return false;
            }
            context.DbUsers.Add(user);
            context.SaveChanges();
            return true;
        }

        public bool AddProduct(Product product)
        {
            foreach (Product tempProduct in Products)
            {
                if (tempProduct.Name.CompareTo(product.Name) == 0)
                    return false;
            }
            context.DbProducts.Add(product);
            context.SaveChanges();
            return true;
        }

        public void UpdateProduct(Product product)
        {
            Product currentProduct = Products.Single(p => p.ProductId == product.ProductId);
            currentProduct.RemoveCategories();
            currentProduct.UpdateValues(product);
            context.SaveChanges();
        }

        public bool RemoveProduct(Product product)
        {
            context.DbProducts.Remove(product);
            context.SaveChanges();
            return true;
        }

        public bool RemoveInventory(int productId, int quantity)
        {
            Product product = Products.Single(p => p.ProductId == productId);
            if (product.removeInventory(quantity))
            {
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool AddCategory(Category category)
        {
            foreach (Category tempCategory in Categories)
            {
                if (tempCategory.Name.CompareTo(category.Name) == 0)
                    return false;
            }
            context.DbCategories.Add(category);
            context.SaveChanges();
            return true;
        }
    }
}
