# C335-Game "ASCII Invaders"


## Group Members

* Jeremy Stephens
* Nathaniel Bowers
* Kristopher O'Bryan

For the solution to work, place the ASCII-Invaders & libs folders into your C:\Irvine directory. If Irvine was extracted somewhere else then you'll have to change the paths in the project settings.
