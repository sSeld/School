// main.cpp
#include <iostream>
#include <iomanip>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <sstream>
#include <vector>
#include <map>
#include <fstream>

using namespace std;
using namespace sf;
/////////////////////////////////////////////////////////
// **** GAME INFORMATION ****
// ---------------------------
// Creators : Jeremy Stephens, Kris O'Bryan, Nate Bowers
/////////////////////////////////////////////////////////
// SOURCE FILES
// GameLoop.cpp
// Main.asm
//
// GAMESTATE
// 0 = GameOver
// 1 = Title Screen
// 2 = Classic mode
// 3 = Classic wave change
// 4 = Scores 
// 5 = Arcade wave change 
// 6 = Scores input
//
// INITIAL VALUES
// Enemy Firing Delay : 5.00 Secs
// Player Firing Delay : 1.00 Secs
/////////////////////////////////////////////////////////


/////////////////////////////////
//CONSTANTS 
/////////////////////////////////
// PLAYER
#define BULLET_VELOCITY 10 // Default: 3
// ENEMY
#define FIRE_DELAY 5.00f  // 5 Seconds
#define ENEMY_MOVE_SPEED 15 // Down: Faster, Up: Slower
// OTHER
#define WAIT_TIME 5.00f
#define BUNKER_HEALTH 5
/////////////////////////////////

//Declaring Shapes
RectangleShape player(Vector2f(50, 50));
RectangleShape bullet(Vector2f(2, 10));
RectangleShape enemybulletrec(Vector2f(2, 10));
RectangleShape playerbulletrec(Vector2f(2, 10));
RectangleShape backgroundRec(Vector2f(800, 600));
RectangleShape backgroundStartScreen(Vector2f(800, 600));
RectangleShape playerRecA(Vector2f(16, 20));
RectangleShape playerRecB(Vector2f(50, 25));
RectangleShape classicRec(Vector2f(350, 99));
RectangleShape classicRecM(Vector2f(350, 99));
RectangleShape arcadeRec(Vector2f(350, 99));
RectangleShape arcadeRecM(Vector2f(350, 99));
RectangleShape scoresRec(Vector2f(350, 99));
RectangleShape scoresRecM(Vector2f(350, 99));
RectangleShape arcadeButRec(Vector2f(300, 115));
vector<RectangleShape> enemyVec;
vector<RectangleShape> bunkerVec;
vector<int> bunkerVecVal;
map<int, string> highScores;
map<int, string> arcadeScores;
Texture explosionArr[9] = {};

//BUNKER Array
Texture bunkerImage[9] = {};

//Variables
int enemyCount = 40;
int GameState = 1;
int Lives = 3;
int Score;
int defUp = 0; // Arcade
int offUp = 0; // Arcade
int bunkerUpCount = BUNKER_HEALTH; // Arcade
int Wave = 1;
int enemySpeed = 2;
int velocityUpgrade = 0;
float fireDelay = 5.00f;
float pFireDelay = 1.00f; // Arcade
bool isFired = false;
bool enemyFired = false;
bool movingLeft = true;
bool testMode = false;
bool dying = false;
bool arcadeMode = false;

//Sound
Sound laser, enemyLaser, explosion;

//Background music
Music gameMusic;

//Enemy firing timer
Time playerFireDelay = seconds(pFireDelay);
Clock playerFireDelayClock;
Time startingFireDelay = seconds(1);
Clock startingFireDelayClock;

//Text
string printBunkerCount = std::to_string(bunkerUpCount + 1);
Text bunkerCountText;

struct enemyBullet {
     int enemyNumber = rand() % enemyCount;
	 int enemyVelocity = rand() % 8 + 1;
     Vector2f enemyVectorCoor = enemyVec.at(enemyNumber).getPosition();
     int bulletX = enemyVectorCoor.x + 25;
     int bulletY = enemyVectorCoor.y + 45;
     int originX = (enemyVectorCoor.x + 25) + 1;
     int originY = (enemyVectorCoor.y + 45) + 5;
};
vector<enemyBullet> bulletsFired;
struct playerBullet {
     bool collided = false;
     int playerVelocity = BULLET_VELOCITY + velocityUpgrade;
     int bulletX = player.getPosition().x + 25;
     int bulletY = player.getPosition().y;
     int originX = (player.getPosition().x + 25) + 1;
     int originY = (player.getPosition().y + 45) + 5;
};
vector<playerBullet> playerBulletsFired;
vector<RectangleShape> playerBulletsFiredDraw;

extern "C" {
     void asmMain();
     void startGameLoop();

     // Assembly
	int UpdateScore();
	int DecLives();
	int getLives();
	void setLives(int l);
	int getShipsx();
	int getShipsy();
	int CheckCollision(int sx,int sy,int bx,int by);
	int CheckCol(float et, float bt);
	int MoveX();
	int MoveY();
	int ModFunc(int a, int b);
	int getLargest(vector<RectangleShape>& v);
	void ClearScore();
     int checkPlayerCollisionPruningAsm(int bulletY);
     int checkPlayerCollisionTopBoxAsm(int originX, int originY, int x, int y);
     int checkPlayerCollisionBottomBoxAsm(int originX, int originY, int x, int y);
     int checkBunkerCollisionPruningAsm(int bulletY);
     int checkBunkerCollisionBOnePruningAsm(int bulletX);
     int checkBunkerCollisionBOneAsm(int originX, int originY, int x, int y);
     int checkBunkerCollisionBTwoPruningAsm(int bulletX);
     int checkBunkerCollisionBTwoAsm(int originX, int originY, int x, int y);
     int checkBunkerCollisionBThreePruningAsm(int bulletX);
     int checkBunkerCollisionBThreeAsm(int originX, int originY, int x, int y);
     int checkBunkerCollisionBFourPruningAsm(int bulletX);
     int checkBunkerCollisionBFourAsm(int originX, int originY, int x, int y);
     // C++
	void initBunkers();
	void makeBunker(int start);
	void InitializeEnemies(Texture t);
	void startGameMusic();
     void checkPlayerInput();
     int checkUpgradeInput();
     bool checkPlayerCollision(enemyBullet b);
     bool checkBunkerCollision(enemyBullet b);
     void loadScores();
	void fireBullet(void);
	void fireEnemyBullet();
	int bulletX, bulletY;     
}


// program entry point
void main()
{
     asmMain();               // call ASM procedure 
}


void startGameLoop()
{
     cout << "Starting Window..." << endl;
     /////////////////////
     // Setting up window
     /////////////////////
     RenderWindow window(VideoMode(800, 600), "ASCII Invaders", Style::Fullscreen);
     window.setFramerateLimit(60);
     Vector2u windowCoor = window.getSize();
	
	//Window icon
	
	Image winicon;
	if (!winicon.loadFromFile("windowicon.png"))
	    cout << "Error loading icon" << endl;
	window.setIcon(winicon.getSize().x, winicon.getSize().y, winicon.getPixelsPtr());
	

	//Lives array
	RectangleShape livesarr[3] = {RectangleShape(Vector2f(20,20)), RectangleShape(Vector2f(20,20)), RectangleShape(Vector2f(20,20))  };

     /////////
     //Player
     ///////////////////////////////////
     // Texture
	// Load Content
     ///////////////////////////////////
     Texture x86;
     if (!x86.loadFromFile("x86.png"))
          cout << "Error loading texture" << endl;
     player.setTexture(&x86);
     player.setPosition(375, 550);

	//PLAYER DEATH SPRITE
	Texture boom;
	if (!boom.loadFromFile("boomsheet.png"))
	    cout << "Error loading explosion texture" << endl;
	Sprite death;
	sf::IntRect deathRec(0, 0, 91, 91);
	death.setTexture(boom);
	death.setTextureRect(deathRec);

     Texture startScreenImg;
     if (!startScreenImg.loadFromFile("startScreen.png"))
          cout << "Error loading texture" << endl;
     backgroundStartScreen.setTexture(&startScreenImg);
     backgroundStartScreen.setPosition(0, 0);

     // Load in ARCADE BUTTONS texture
     Texture arcadeButtons;
     if (!arcadeButtons.loadFromFile("arcadeButtons.png"))
          cout << "Error loading texture" << endl;
     arcadeButRec.setTexture(&arcadeButtons);
     arcadeButRec.setPosition(250, 100);

     Texture classic, classicM, arcade, arcadeM, scores, scoresM;
     // Load in CLASSIC texture
     if (!classic.loadFromFile("classic.png"))
          cout << "Error loading texture" << endl;
     classicRec.setTexture(&classic);
     classicRec.setPosition(400, 200);
     if (!classicM.loadFromFile("classicM.png"))
          cout << "Error loading texture" << endl;
     classicRecM.setTexture(&classicM);
     classicRecM.setPosition(400, 200);

     // Load in ARCADE texture
     if (!arcade.loadFromFile("arcade.png"))
          cout << "Error loading texture" << endl;
     arcadeRec.setTexture(&arcade);
     arcadeRec.setPosition(400, 300);
     if (!arcadeM.loadFromFile("arcadeM.png"))
          cout << "Error loading texture" << endl;
     arcadeRecM.setTexture(&arcadeM);
     arcadeRecM.setPosition(400, 300);

     // Load in SCORES texture
     if (!scores.loadFromFile("scores.png"))
          cout << "Error loading texture" << endl;
     scoresRec.setTexture(&scores);
     scoresRec.setPosition(400, 400);
     if (!scoresM.loadFromFile("scoresM.png"))
          cout << "Error loading texture" << endl;
     scoresRecM.setTexture(&scoresM);
     scoresRecM.setPosition(400, 400);

     //Load Score chart for Scores Page
     Texture hsBox;
     if (!hsBox.loadFromFile("scorebox.png"))
          cout << "Error loading scorebox.png" << endl;

	//BACKGROUND
	Texture BackgroundTexture;
	if (!BackgroundTexture.loadFromFile("background.jpg"))
	    cout << "Error loading background" << endl;
	backgroundRec.setTexture(&BackgroundTexture);
	backgroundRec.setPosition(0, 0);

	//ENEMY TEXTURE
     Texture enemytex, enemytex2;
     if (!enemytex.loadFromFile("enemy1.png"))
          cout << "Error loading enemy texture" << endl;
     if (!enemytex2.loadFromFile("enemy2.png"))
          cout << "Error loading enemy texture 2" << endl;

     //Position Player Debug Rectangles
     playerRecA.setFillColor(Color(0, 225, 0));
     playerRecB.setFillColor(Color(0, 225, 0));
     playerRecA.setPosition(player.getPosition().x + 17, player.getPosition().y);
     playerRecB.setPosition(player.getPosition().x, player.getPosition().y + 20);

	//CREATE WALLS
	RectangleShape WallL(Vector2f(50, 500));
	RectangleShape WallR(Vector2f(50, 500));
	WallL.setFillColor(Color(100, 0, 0));
	WallL.setPosition(200, 25);
	WallR.setFillColor(Color(100, 0, 0));
	WallR.setPosition(550, 25);

	//CALL ENEMY FUNCTION HERE
	InitializeEnemies(enemytex);

	//BUNKER TEXTURE
	for (int i = 0; i < 9; i++)
	{
		if (!bunkerImage[i].loadFromFile("bunker" + std::to_string(i) + ".png"))
			cout << "Error loading bunker texture" << endl;
	}

	//BUNKER CREATIONS
	initBunkers();

	//POSITION BUNKERS
	makeBunker(0);
	makeBunker(9);
	makeBunker(18);
	makeBunker(27);

     //////////////////////////////////////////
     // TEXT & FONT
     //////////////////////////////////////////
	//FONT
	Font font;
	if (!font.loadFromFile("consola.ttf"))
	    cout << "Error loading font" << endl;

     //AUTHORS (MAIN MENU)
     Text authorsText;
     authorsText.setFont(font);
     authorsText.setCharacterSize(12);
     authorsText.setColor(sf::Color::Black);
     authorsText.setStyle(sf::Text::Bold);
     authorsText.setPosition(10, 580);
     authorsText.setString("Authors: Jeremy Stephens, Kris O'Bryan, Nate Bowers");

	//SCORING
	string printscore = "Score: " + std::to_string(Score);
	Score = 0;
	Text scoretext;
	scoretext.setFont(font);
	scoretext.setCharacterSize(14);
	scoretext.setColor(sf::Color::White);
	scoretext.setStyle(sf::Text::Bold);
	scoretext.setPosition(10, 20);
	scoretext.setString(printscore);

	//LIVES
	Text livestext;
	livestext.setFont(font);
	livestext.setCharacterSize(14);
	livestext.setColor(sf::Color::White);
	livestext.setStyle(sf::Text::Bold);
	livestext.setPosition(10, 40);
	livestext.setString("Lives: ");

     //WAVE (ONSCREEN)
     string printOnscreenWaveNum = "Wave: " + std::to_string(Wave);
     Text waveOnscreenText;
     waveOnscreenText.setFont(font);
     waveOnscreenText.setCharacterSize(14);
     waveOnscreenText.setColor(sf::Color::White);
     waveOnscreenText.setStyle(sf::Text::Bold);
     waveOnscreenText.setPosition(10, 60);
     waveOnscreenText.setString(printOnscreenWaveNum);

     //UPGRADES (ARCADE) & UPGRADES INFO
     string printUpgrades = "Defense Upgrades: " + std::to_string(defUp) + "  Offense Upgrades: " + std::to_string(offUp);
     Text upgradesText;
     upgradesText.setFont(font);
     upgradesText.setCharacterSize(14);
     upgradesText.setColor(sf::Color::White);
     upgradesText.setStyle(sf::Text::Bold);
     upgradesText.setPosition(425, 20);
     upgradesText.setString(printUpgrades);

     string printUpgradesInfo = std::to_string(bunkerUpCount + 1) + " Bunker Health            " + std::to_string(pFireDelay) + " Fire Rate";
     Text upgradesTextInfo;
     upgradesTextInfo.setFont(font);
     upgradesTextInfo.setCharacterSize(10);
     upgradesTextInfo.setColor(sf::Color::Green);
     upgradesTextInfo.setStyle(sf::Text::Bold);
     upgradesTextInfo.setPosition(425, 40);
     upgradesTextInfo.setString(printUpgradesInfo);

     //BUNKER COUNT
     bunkerCountText.setFont(font);
     bunkerCountText.setCharacterSize(15);
     bunkerCountText.setColor(sf::Color::White);
     bunkerCountText.setStyle(sf::Text::Bold);
     bunkerCountText.setPosition(1000, 1000);
     bunkerCountText.setString(printBunkerCount);

	for (auto &rec : livesarr)
	    rec.setTexture(&x86);

	livesarr[0].setPosition(70, 35);
	livesarr[1].setPosition(90, 35);
	livesarr[2].setPosition(110, 35);

	int jump = 0; //Determines when enemies should move
	Texture swap;
	int shiftx; //move enemies x
	int shifty; //move enemies y
	int FrontX = 0; //gets front x
	int LastX = 7; //gets top right x
	Vector2f LastVec(enemyVec.at(7).getPosition().x, enemyVec.at(7).getPosition().y);
	Vector2f NewVec;
	int largestX = 0;
     int largestY = 0;
	int smallestX = 0;
	int speedset = 45;

	//Enemy firing timer
	Time enemyFireDelay = seconds(fireDelay);
	Clock clock;

	//Screen Delay
	Time waitTime = seconds(WAIT_TIME);
	Clock screenClock;

	//DethKolk
	Clock deathClock;
	Clock aniClock;

	//Music
	Music music;
	if (!music.openFromFile("titlemusic.ogg"))
	{
	    // error...
	}

	//Load GameMusic
	if (!gameMusic.openFromFile("pantera.ogg"))//"Pantera.ogg"))
	{
	    // error...
	}

     //Load InputMenu Music
     Music inputMusic;
     if (!inputMusic.openFromFile("floods.ogg"))
     {
          // error...
     }

	//Sound Buffers
	SoundBuffer buffer, buffer2, buffer3;
	if (!buffer.loadFromFile("laser1.ogg"))
	    cout << "Error loading laser1 sound" << endl;
	laser.setBuffer(buffer);
	if (!buffer2.loadFromFile("enemylaser.ogg"))
	    cout << "Error loading enemylaser sound" << endl;
	enemyLaser.setBuffer(buffer2);
	if (!buffer3.loadFromFile("explosion.ogg"))
	    cout << "Error loading explosion sound" << endl;
	explosion.setBuffer(buffer3);

	int dyingFrames = 0;

	RestartGame:
	music.play();

     PlayGameMusic:
	if (GameState == 2)
	{
	    gameMusic.setLoop(true);
	    gameMusic.play();
	}

     InputMenu:
     if (GameState == 6)
     {
          inputMusic.setLoop(true);
          inputMusic.play();
     }

     //Enter Name Page Variables
     RectangleShape textBox(Vector2f(200, 25));
     textBox.setPosition(300, 400);

     Text userText, menuText;
     userText.setCharacterSize(14);
     userText.setFont(font);
     userText.setColor(Color(0, 0, 255));
     userText.setPosition(300, 400);

     string nameInput; //what the user enters
     string modInput; //when the user hits backspace

     //////////////////
     //Main Game Loop
     //////////////////
     while (window.isOpen())
     {
	    Event event;
         string path;
	    while (window.pollEvent(event))
	    {
		   if (event.type == Event::Closed || Keyboard::isKeyPressed(Keyboard::Escape))
			  window.close();
             ////////////////////
             // Enter names state
             ////////////////////
             if (GameState == 6)
             {
                  if (arcadeMode == true)
                       path = "arcade.txt";
                  else
                       path = "classic.txt";

                  if (event.type == sf::Event::TextEntered)
                  {
                       if (event.text.unicode == '\b' && nameInput.size() != 0)
                       {
                            nameInput.erase(nameInput.size() - 1);
                       }
                       else if (event.text.unicode < 128 && event.text.unicode != '\b')
                       {
					  if(!(nameInput.length() > 22))
                            nameInput += event.text.unicode;
                       }
                       userText.setString(nameInput);
                  } 
                  else if (event.type == sf::Event::KeyPressed)
                  {
                       if (event.key.code == sf::Keyboard::Return)
                       {
                            if (nameInput.length() != 0)
                            {
                                 ofstream file;
                                 file.open(path, std::ios_base::app);

                                 file << nameInput << "\n";
                                 file << Score << "\n";
                                 file.close();
                            }

                            Score = 0; //sets the score to 0
                            ClearScore(); //clears the assembly score data
                            printscore = "Score: " + std::to_string(Score); //sets score back to 0
                            scoretext.setString(printscore);
                            GameState = 1; //Go back to title screen
                            inputMusic.stop();
                            goto RestartGame;
                       }
                  }
             }
	    }
	    

	    ///////////////////
	    //Check Gamestates
	    ///////////////////
         
	    if (GameState == 2) //GAMEPLAY
	    {
		   // If bullet flies off screen
		   if (bullet.getPosition().y < 0)
		   {
			  isFired = false;
		   }

		   //Enemy speed setter
		   if (enemyCount <= 40 && enemyCount >= 36)
			  enemySpeed = 20;
		   else if (enemyCount <= 35 && enemyCount >= 30)
			  enemySpeed = 15;
		   else if (enemyCount <= 29 && enemyCount >= 24)
			  enemySpeed = 13;
		   else if (enemyCount <= 23 && enemyCount >= 18)
			  enemySpeed = 10;
		   else if (enemyCount <= 17 && enemyCount >= 12)
			  enemySpeed = 8;
		   else if (enemyCount <= 11 && enemyCount >= 6)
			  enemySpeed = 6;
		   else
			  enemySpeed = 4;

		   if (arcadeMode == false)
		   {
		        //Check Enemy collision with bullet
		        if (isFired == true)
		        {
			       for (int i = 0; i < enemyVec.size(); i++)
			       {
				      if (enemyVec.at(i).getGlobalBounds().intersects(bullet.getGlobalBounds()))
				      {
					     isFired = false;
					     bullet.setPosition(0, 0); //This can be changed, this fixes a glitch where an inactive bullet was erasing enemies
					     enemyVec.erase(enemyVec.begin() + i);
					     explosion.play();
					     enemyCount -= 1;
					     Score = UpdateScore();
					     printscore = "Score: " + std::to_string(Score);
					     scoretext.setString(printscore);
					     if (enemyVec.size() == 0)
					     {
					         Wave += 1;
					         screenClock.restart();
					         GameState = 3;
					     }
				      }
			       }
		        }

		        //bunker collision
		        for (int j = 0; j < bunkerVec.size(); j++)
		        {
			       if (bunkerVec.at(j).getGlobalBounds().intersects(bullet.getGlobalBounds()))
			       {
				      isFired = false;
				      bullet.setPosition(0, 0);
				      //playerBulletsFired[i].collided = true;
				      if (bunkerVecVal[j] == 0)
					     bunkerVec.at(j).setPosition(Vector2f(1000, 1000));
				      else
					     bunkerVecVal[j]--;
				      bunkerVec[j].setFillColor(Color(255, 96, 16, 255 - (255 / (bunkerVecVal[j] + 2))));
                          
			       }
		        }
	         }

		   //get largest x in enemyvec
		   int posx = 0, posy = 0;
		   largestX = 0;
             largestY = 0;
		   smallestX = 800;
		   for (int r = 0; r < enemyVec.size(); r++)
		   {
			  posx = enemyVec.at(r).getPosition().x;
                 posy = enemyVec.at(r).getPosition().y;
			  if (posx < smallestX)
				 smallestX = posx;
			  if (posx > largestX)
				 largestX = posx;
                 if (posy > largestY)
                      largestY = posy;
		   }

             if (largestY+50 > player.getPosition().y)
                  goto Dead;

		   //Move Enemies
		   if (ModFunc(jump, enemySpeed) == 0)
		   {
			  for (int i = 0; i < enemyCount; i++)
			  { 
				 shiftx = MoveX();
				 //Check if Y should shift
				 if (WallR.getPosition().x > 750)
				 {
					WallR.setPosition(760, WallR.getPosition().y);
					movingLeft = true;
					enemyVec.at(i).setPosition(enemyVec.at(i).getPosition().x, enemyVec.at(i).getPosition().y + MoveY());
				 }
				 if (WallL.getPosition().x <= 0)
				 {
					WallL.setPosition(0, WallL.getPosition().y);
					movingLeft = false;
					enemyVec.at(i).setPosition(enemyVec.at(i).getPosition().x, enemyVec.at(i).getPosition().y + MoveY());
				 }

				 //Checks which direction enemies should move in
				 if (movingLeft == false)
					enemyVec.at(i).setPosition(enemyVec.at(i).getPosition().x + shiftx, enemyVec.at(i).getPosition().y);
				 else
					enemyVec.at(i).setPosition(enemyVec.at(i).getPosition().x - shiftx, enemyVec.at(i).getPosition().y);

				 

				 //Toggle texture with each movement
				 if (enemyVec.at(i).getTexture() == &enemytex)
					enemyVec.at(i).setTexture(&enemytex2);
				 else
					enemyVec.at(i).setTexture(&enemytex);

			  }
		   }
             jump++;
             WallL.setPosition(smallestX, WallL.getPosition().y); //sets left wall to enemy in front
             WallR.setPosition(largestX, WallR.getPosition().y);   //sets right wall to top right enemy
		   
	    }//END GAMESTATE CHECK FOR UPDATE

          window.clear();
          //cout << "bunkerUpCount: " << bunkerUpCount << endl;
		//////////////
          //Drawing area
		//////////////
          // Gamestate Main Menu
          if (GameState == 1)
          {
               window.draw(backgroundStartScreen);
               window.draw(classicRec);
               window.draw(arcadeRec);
               window.draw(scoresRec);
               window.draw(authorsText);
               Vector2i mouseCoor = Mouse::getPosition(window);        
               // Classic mouseover check
               if ((mouseCoor.x >= 400 && mouseCoor.x <= 750) && (mouseCoor.y >= 200 && mouseCoor.y <= 299))
               {
                    window.draw(classicRecM);
                    if (Mouse::isButtonPressed(Mouse::Button::Left))
                    {
                         screenClock.restart();
					music.stop();
                         arcadeMode = false;
                         GameState = 3;
                    }
               }
               // Arcade mouseover check
               if ((mouseCoor.x >= 400 && mouseCoor.x <= 750) && (mouseCoor.y >= 300 && mouseCoor.y <= 399))
               {
                    window.draw(arcadeRecM);
                    if (Mouse::isButtonPressed(Mouse::Button::Left))
                    {
                         startingFireDelayClock.restart();
                         screenClock.restart();
                         music.stop();
                         arcadeMode = true;
                         GameState = 3;
                    }
               }
               // Scores mouseover check
               if ((mouseCoor.x >= 400 && mouseCoor.x <= 750) && (mouseCoor.y >= 400 && mouseCoor.y <= 499))
               {
                    window.draw(scoresRecM);
                    if (Mouse::isButtonPressed(Mouse::Button::Left))
                    {
                         
                         //screenClock.restart();
                         GameState = 4;
                    }
               }
          }
          // Gamestate Classic Mode
		if (GameState == 2)
		{
		    window.draw(backgroundRec);
		    checkPlayerInput();
		    window.draw(scoretext);
		    window.draw(livestext);
              window.draw(waveOnscreenText);
              if (arcadeMode)
              {
                   window.draw(upgradesText);
                   window.draw(upgradesTextInfo);
                   window.draw(bunkerCountText);
              }
                   

		    //////////////////
		    // Death Animation
		    //////////////////

		    if (dying == true && deathClock.getElapsedTime().asSeconds() > .005f)
		    {
			 
		          if (deathRec.left == 455 && deathRec.top == 0)
				{
					 deathRec.left = 0;
					 deathRec.top += 91;
				}
				else if (deathRec.left == 455 && deathRec.top == 91)
				{
					 deathRec.left = 0;
					 deathRec.top = 0;
				}
				else
				{
					 deathRec.left += 91;
				}

				death.setPosition(player.getPosition().x-20,player.getPosition().y-20);
				death.setTextureRect(deathRec);
				deathClock.restart();
				if (aniClock.getElapsedTime().asSeconds() > .35f)
				{
					 dying = false;
					 player.setPosition(375, 550);
					 playerRecA.setPosition(player.getPosition().x + 17, player.getPosition().y);
					 playerRecB.setPosition(player.getPosition().x, player.getPosition().y + 20);
				}

			   window.draw(death);
		    }

		    if (testMode == true)
		    {
			   window.draw(WallL); //TEST MODE
			   window.draw(WallR);
			   window.draw(playerRecA);
			   window.draw(playerRecB);
		    }

			for (auto &rec : enemyVec)
				window.draw(rec);
			for (auto &rec : bunkerVec)
				window.draw(rec);
			if (!dying)
			    window.draw(player);

		    //Draw lives
		    for (int i = 0; i < getLives(); i++)
			   window.draw(livesarr[i]);

		    //handle firing (player)
              // Fire all player bullets
		    if (arcadeMode == true)
		    {
              for (int i = 0; i < playerBulletsFired.size(); ++i)
              {
                   playerBulletsFiredDraw[i].setPosition(playerBulletsFired[i].bulletX, playerBulletsFired[i].bulletY);
                   playerBulletsFired[i].bulletY -= playerBulletsFired[i].playerVelocity;
                   playerBulletsFired[i].originY -= playerBulletsFired[i].playerVelocity;
                   window.draw(playerBulletsFiredDraw[i]);
              }

              //Check which ones are offscreen or collided with player
		    for (int i = 0; i < playerBulletsFired.size(); i++)
		    {
			   //bunker collision
			   for (int j = 0; j < bunkerVec.size(); j++)
			   {
				  if (bunkerVec.at(j).getGlobalBounds().intersects(playerBulletsFiredDraw[i].getGlobalBounds()))
				  {
					 //isFired = false;
					 //bullet.setPosition(0, 0);
					 playerBulletsFired[i].collided = true;
					 if (bunkerVecVal[j] == 0)
						bunkerVec.at(j).setPosition(Vector2f(1000, 1000));
					 else
						bunkerVecVal[j]--;
                          bunkerVec[j].setFillColor(Color(255, 96, 16, 255 - (255 / (bunkerVecVal[j] + 2))));

				  }
			   }


			   //Check Enemy collision with bullet
			   
				  for (int k = 0; k < enemyVec.size(); k++)
				  {
					 if (enemyVec.at(k).getGlobalBounds().intersects(playerBulletsFiredDraw[i].getGlobalBounds()))
					 {

						//isFired = false;
						//bullet.setPosition(0, 0); //This can be changed, this fixes a glitch where an inactive bullet was erasing enemies
						enemyVec.erase(enemyVec.begin() + k);
						playerBulletsFired[i].collided = true;
						explosion.play();
						enemyCount -= 1;
						Score = UpdateScore();
						printscore = "Score: " + std::to_string(Score);
						scoretext.setString(printscore);
						if (enemyVec.size() == 0)
						{
						    Wave += 1;
						    screenClock.restart();
						    GameState = 5;
						}
					 }
				  }
				  if (playerBulletsFired[i].collided == true)
				  {
					 playerBulletsFired.erase(playerBulletsFired.begin() + i);
					 playerBulletsFiredDraw.erase(playerBulletsFiredDraw.begin() + i);
				  }
				  // Bullet offscreen delete
				  else if (playerBulletsFired[i].bulletY < 0)
				  {
					 playerBulletsFired.erase(playerBulletsFired.begin() + i);
					 playerBulletsFiredDraw.erase(playerBulletsFiredDraw.begin() + i);
				  }

			   }
		    
		    }
		    else
		    {
			   //handle firing (player)
			   if (isFired == true)
			   {
				  bullet.setPosition(bulletX, bulletY);
				  bulletY -= BULLET_VELOCITY;
				  window.draw(bullet);
			   }
		    }
                   
		    //Handle enemy firing
		    if (clock.getElapsedTime() > enemyFireDelay)
		    {
			   fireEnemyBullet();
			   clock.restart();
		    }
              // Fire all the bullets
		    for (vector<enemyBullet>::iterator i = bulletsFired.begin(); i != bulletsFired.end(); ++i)
		    {
			   enemybulletrec.setPosition(i->bulletX, i->bulletY);
			   i->bulletY += i->enemyVelocity;
                  i->originY += i->enemyVelocity;
			   window.draw(enemybulletrec);
		    }

		    //Check which ones are offscreen or collided with player
		    for (int i = 0; i < bulletsFired.size(); i++)
		    {
                  if (checkBunkerCollision(bulletsFired.at(i)))
                  {
                       //Bunker hit
                       bulletsFired.erase(bulletsFired.begin() + i);
                       
                  }
                  else if (checkPlayerCollision(bulletsFired.at(i)))
                  {
				   //dying = true;
                        bulletsFired.erase(bulletsFired.begin() + i);
                        //Decrement lives
				    DecLives();
				    dying = true;
				    explosion.play();
				    bulletsFired.clear();
				    aniClock.restart();
				    if (getLives() == 0) //checks if lives is 0 to jump to gamestate 0
				    {
                            Dead:
					   screenClock.restart(); //start the clock for gamestate 0
					   scoretext.setString(printscore); //resets score text
					   GameState = 0; //jump to gameover state
				    }
                  }
			   else if (bulletsFired.at(i).bulletY > 600)
				  bulletsFired.erase(bulletsFired.begin() + i);
		    } 
              if (testMode)
              {
                   //Draw hit counter for bunkers
                   for (int i = 0; i < bunkerVec.size(); i++)
                   {                                                          
                        printBunkerCount = std::to_string(bunkerVecVal[i]);
                        bunkerCountText.setPosition(bunkerVec[i].getPosition().x + 6, bunkerVec[i].getPosition().y + 3);
                        bunkerCountText.setString(printBunkerCount);
                        window.draw(bunkerCountText);
                   }
              }
		}
          // GameState Gameover
		if (GameState == 0)
		{
		    gameMusic.stop();
		    Text gameovertext;
		    gameovertext.setFont(font);
		    gameovertext.setCharacterSize(100);
		    gameovertext.setColor(sf::Color::White);
		    gameovertext.setStyle(sf::Text::Bold);
		    gameovertext.setPosition(155, 200);
		    gameovertext.setString("GAME OVER");
		    window.draw(gameovertext);
		    if (screenClock.getElapsedTime() > waitTime)
		    {
			   Wave = 1;
			   setLives(3);
			   bunkerVec.clear();
                  bunkerVecVal.clear();
			   enemyVec.clear();
			   enemyCount = 40;
			   InitializeEnemies(enemytex);
                  offUp = 0;
                  defUp = 0;
                  bunkerUpCount = BUNKER_HEALTH;
			   initBunkers();
			   makeBunker(0);
			   makeBunker(9);
			   makeBunker(18);
			   makeBunker(27);
                  pFireDelay = 1.00f;
                  fireDelay = 5.00f;
                  startingFireDelayClock.restart();
                  enemyFireDelay = seconds(fireDelay);
                  playerFireDelay = seconds(pFireDelay);
			   bulletsFired.clear();
			   player.setPosition(375, 550);
			   playerRecA.setPosition(player.getPosition().x + 17, player.getPosition().y);
			   playerRecB.setPosition(player.getPosition().x, player.getPosition().y + 20);
			   screenClock.restart();
			   GameState = 6;
			   goto InputMenu;
		    }
		}
		
		// Gamestate change waves
		if (GameState == 3)
		{
		    Text wavetext;
		    wavetext.setFont(font);
		    wavetext.setCharacterSize(100);
		    wavetext.setColor(sf::Color::Green);
		    wavetext.setStyle(sf::Text::Bold);
		    wavetext.setPosition(240, 200);
		    wavetext.setString("Wave " + std::to_string(Wave));
		    window.draw(wavetext);
              printUpgrades = "Defense Upgrades: " + std::to_string(defUp) + "  Offense Upgrades: " + std::to_string(offUp);
              upgradesText.setString(printUpgrades);
              printUpgradesInfo = std::to_string(bunkerUpCount + 1) + " Bunker Health            " + std::to_string(pFireDelay) + " Fire Rate";
              upgradesTextInfo.setString(printUpgradesInfo);
              string printOnscreenWaveNum = "Wave: " + std::to_string(Wave);
              waveOnscreenText.setString(printOnscreenWaveNum);
              movingLeft = false;
		    if (screenClock.getElapsedTime() > waitTime)
		    {
                  startingFireDelayClock.restart();
			   if (Wave != 1)
			   {
				  enemyCount = 40;
                      fireDelay -= (fireDelay * .25);
                      enemyFireDelay = seconds(fireDelay);
				  InitializeEnemies(enemytex);
				  bulletsFired.clear();
                      playerBulletsFired.clear();
                      playerBulletsFiredDraw.clear();
				  player.setPosition(375, 550);
				  playerRecA.setPosition(player.getPosition().x + 17, player.getPosition().y);
				  playerRecB.setPosition(player.getPosition().x, player.getPosition().y + 20);
                      if (Wave % 5 == 0)
                      {
                           bunkerVec.clear();
                           bunkerVecVal.clear();
                           initBunkers();
                           makeBunker(0);
                           makeBunker(9);
                           makeBunker(18);
                           makeBunker(27);
                      }
			   }
			   GameState = 2;
			   if (Wave==1)
				  goto PlayGameMusic;
		    }
		}

          // Arcade wave change
          if (GameState == 5)
          {
               Text wavetext;
               wavetext.setFont(font);
               wavetext.setCharacterSize(100);
               wavetext.setColor(sf::Color::Green);
               wavetext.setStyle(sf::Text::Bold);
               wavetext.setPosition(240, 200);
               wavetext.setString("Wave " + std::to_string(Wave));
               Text waveArcadeText;
               waveArcadeText.setFont(font);
               waveArcadeText.setCharacterSize(40);
               waveArcadeText.setColor(sf::Color::Cyan);
               waveArcadeText.setStyle(sf::Text::Bold);
               waveArcadeText.setPosition(250, 40);
               waveArcadeText.setString("Choose Upgrade");
               window.draw(arcadeButRec);
               window.draw(upgradesText);
               window.draw(upgradesTextInfo);
               window.draw(wavetext);
               window.draw(waveArcadeText);
               string printOnscreenWaveNum = "Wave: " + std::to_string(Wave);
               waveOnscreenText.setString(printOnscreenWaveNum);
               movingLeft = false;
               startingFireDelayClock.restart();
               if (checkUpgradeInput() == 0)
               {
                    offUp++;
                    printUpgrades = "Defense Upgrades: " + std::to_string(defUp) + "  Offense Upgrades: " + std::to_string(offUp);
                    upgradesText.setString(printUpgrades);
                    pFireDelay -= (pFireDelay * .15);
                    playerFireDelay = seconds(pFireDelay);
                    printUpgradesInfo = std::to_string(bunkerUpCount + 1) + " Bunker Health            " + std::to_string(pFireDelay) + " Fire Rate";
                    upgradesTextInfo.setString(printUpgradesInfo);
                    if (Wave != 1)
                    {
                         enemyCount = 40;
                         fireDelay -= (fireDelay * .25);
                         enemyFireDelay = seconds(fireDelay);
                         InitializeEnemies(enemytex);
                         bulletsFired.clear();
                         playerBulletsFired.clear();
                         playerBulletsFiredDraw.clear();
                         player.setPosition(375, 550);
                         playerRecA.setPosition(player.getPosition().x + 17, player.getPosition().y);
                         playerRecB.setPosition(player.getPosition().x, player.getPosition().y + 20);
                         if (Wave % 5 == 0)
                         {
                              bunkerVec.clear();
                              bunkerVecVal.clear();
                              initBunkers();
                              makeBunker(0);
                              makeBunker(9);
                              makeBunker(18);
                              makeBunker(27);
                         }
                    }
                    GameState = 2;
                    if (Wave == 1)
                         goto PlayGameMusic;
               }
               else if (checkUpgradeInput() == 1)
               {
                    defUp++;
                    printUpgrades = "Defense Upgrades: " + std::to_string(defUp) + "  Offense Upgrades: " + std::to_string(offUp);
                    upgradesText.setString(printUpgrades);
                    bunkerUpCount += 1;
                    printUpgradesInfo = std::to_string(bunkerUpCount + 1) + " Bunker Health            " + std::to_string(pFireDelay) + " Fire Rate";
                    upgradesTextInfo.setString(printUpgradesInfo);
                    if (Wave != 1)
                    {
                         enemyCount = 40;
                         fireDelay -= (fireDelay * .25);
                         enemyFireDelay = seconds(fireDelay);
                         InitializeEnemies(enemytex);
                         bulletsFired.clear();
                         playerBulletsFired.clear();
                         playerBulletsFiredDraw.clear();
                         player.setPosition(375, 550);
                         playerRecA.setPosition(player.getPosition().x + 17, player.getPosition().y);
                         playerRecB.setPosition(player.getPosition().x, player.getPosition().y + 20);
                         if (Wave % 5 == 0)
                         {
                              bunkerVec.clear();
                              bunkerVecVal.clear();
                              initBunkers();
                              makeBunker(0);
                              makeBunker(9);
                              makeBunker(18);
                              makeBunker(27);
                         }
                    }
                    GameState = 2;
                    if (Wave == 1)
                         goto PlayGameMusic;
               }
          }
          // SCORES/INPUT
          if (GameState == 4) //SCORES PAGE
          {
               Text fileText;
               fileText.setFont(font);
               fileText.setCharacterSize(14);
               int posx = 48;
               int posy = 114;
               fileText.setPosition(posx, posy);

               Texture scoreBackground;
               RectangleShape blackPallet(Vector2f(300, 400)); //Sits Behind Scores
               blackPallet.setTexture(&hsBox); //loads in the scores box
               blackPallet.setPosition(40, 80);
               blackPallet.setFillColor(Color(255, 255, 255, 100));
               RectangleShape page(Vector2f(800, 600));
               if (!scoreBackground.loadFromFile("scorePage.jpg"))
                    cout << "Error loading background for this page!" << endl;
               page.setTexture(&scoreBackground);
               page.setPosition(0, 0);

               window.draw(page);
               window.draw(blackPallet); //Classic Pallet
               blackPallet.setPosition(460, 80);
               window.draw(blackPallet); //Arcade Pallet

                                         //Headers
               RectangleShape Header(Vector2f(200, 60));
               Header.setTexture(&classic);
               Header.setPosition(86, 5);
               window.draw(Header);
               Header.setTexture(&arcade);
               Header.setPosition(506, 5);
               window.draw(Header);

               //Back Button
               RectangleShape back(Vector2f(96, 46));
               Texture Bb;
               if (!Bb.loadFromFile("backbutton.png"))
                    cout << "error loading back button texture" << endl;
               back.setTexture(&Bb);
               back.setPosition(355, 550);
               back.setFillColor(Color(255, 255, 255, 150));
               window.draw(back);

               Vector2i mouseCoor = Mouse::getPosition(window); //Gets mouse for this screen

               if ((mouseCoor.x >= 355 && mouseCoor.x <= 451) && (mouseCoor.y >= 550 && mouseCoor.y <= 596))
               {
                    if (Mouse::isButtonPressed(Mouse::Button::Left))
                         GameState = 1;
               }

               loadScores();

               vector<pair<int, string>> mapVec;

               for (auto i = highScores.begin(); i != highScores.end(); ++i)
                    mapVec.push_back(*i);
               sort(mapVec.begin(), mapVec.end());

               int bound;
               if (mapVec.size() > 20)
                    bound = mapVec.size() - 20;
               else
                    bound = 0;

               for (int i = mapVec.size() - 1, c = 1; i >= bound; i--, c++)
               {
                    //Write the number
                    fileText.setString(to_string(c));
                    window.draw(fileText);
                    fileText.setPosition(70, posy);

                    //Write the name and score
                    fileText.setString(mapVec[i].second); //gets name
                    window.draw(fileText);
                    fileText.setPosition(270, posy);
                    fileText.setString(to_string(mapVec[i].first)); //gets score
                    window.draw(fileText); //draw the score
                    posx = 48;
                    posy += 14;
                    fileText.setPosition(posx, posy);
               }
               mapVec.clear();

               for (auto i = arcadeScores.begin(); i != arcadeScores.end(); ++i)
                    mapVec.push_back(*i);
               sort(mapVec.begin(), mapVec.end());

               posx = 468;
               posy = 114;
               fileText.setPosition(posx, posy);

               for (int i = mapVec.size() - 1, c = 1; i >= 0; i--, c++)
               {
                    //Write the number
                    fileText.setString(to_string(c));
                    window.draw(fileText);
                    fileText.setPosition(490, posy);

                    //Write the name and score
                    fileText.setString(mapVec[i].second); //gets name
                    window.draw(fileText);
                    fileText.setPosition(690, posy);
                    fileText.setString(to_string(mapVec[i].first)); //gets score
                    window.draw(fileText); //draw the score
                    posx = 468;
                    posy += 14;
                    fileText.setPosition(posx, posy);
               }
          }
          if (GameState == 6) //Game State for getting score (could be combined with GameState 0 but would need code modification)
          {

               menuText.setCharacterSize(42);
               menuText.setFont(font);
               menuText.setPosition(150, 240);
               menuText.setColor(Color(255, 255, 255));
               menuText.setString("Please enter your name:");
               window.draw(menuText);
               window.draw(textBox);
               window.draw(userText);
          }

          // End Drawing area
          window.display();
     }
}
///////////////////////
// Fill Bunker Vector
///////////////////////
void initBunkers()
{

    for (int i = 0; i < 36; i++)
    {
        bunkerVecVal.push_back(bunkerUpCount);
	   bunkerVec.push_back(RectangleShape(Vector2f(22, 22)));
	   bunkerVec.at(i).setFillColor(Color(255, 96, 16));
	   if (i == 0 || i == 9 || i == 18 || i == 27)
		  bunkerVec.at(i).setTexture(&bunkerImage[0]);
	   if (i == 1 || i == 10 || i == 19 || i == 28)
		  bunkerVec.at(i).setTexture(&bunkerImage[1]);
	   if (i == 2 || i == 11 || i == 20 || i == 29)
		  bunkerVec.at(i).setTexture(&bunkerImage[2]);
	   if (i == 3 || i == 12 || i == 21 || i == 30)
		  bunkerVec.at(i).setTexture(&bunkerImage[3]);
	   if (i == 4 || i == 13 || i == 22 || i == 31)
		  bunkerVec.at(i).setTexture(&bunkerImage[4]);
	   if (i == 5 || i == 14 || i == 23 || i == 32)
		  bunkerVec.at(i).setTexture(&bunkerImage[5]);
	   if (i == 6 || i == 15 || i == 24 || i == 33)
		  bunkerVec.at(i).setTexture(&bunkerImage[6]);
	   if (i == 7 || i == 16 || i == 25 || i == 34)
		  bunkerVec.at(i).setTexture(&bunkerImage[7]);
	   if (i == 8 || i == 17 || i == 26 || i == 35)
		  bunkerVec.at(i).setTexture(&bunkerImage[8]);
    }
}

///////////////////////
// Make Bunker
///////////////////////
void makeBunker(int start)
{
	int posx;
	int savex;
	int posy = 450;
	if (start == 0)
		savex = 100;
	if (start == 9)
		savex = 275;
	if (start == 18)
		savex = 450;
	if (start == 27)
		savex = 625;
	for (int i = start; i < start + 9; i++)
	{
		if (ModFunc(i, 3) == 0)
		{
			posy += 22;
			posx = savex;
		}
		bunkerVec.at(i).setPosition(Vector2f(posx, posy));
		posx += 22;
	}
}

///////////////////////
// Initialize Enemies
///////////////////////
void InitializeEnemies(Texture et)
{
    //FORM ENEMY RECTANGLES
    for (int i = 0; i < enemyCount; i++)
	   enemyVec.push_back(RectangleShape(Vector2f(50, 50)));

    for (auto &rec : enemyVec)
    {
	   rec = RectangleShape(Vector2f(50, 50));
	   rec.setTexture(&et);
    }

    //POSITION ENEMY RECTANGLES
    float posx = 200;
    float posy = 25;

    int j = 0;
    int row = 0;
    for (auto &rec : enemyVec)
    {
	   if (ModFunc(j, 8) == 0)
	   {
		  posy += 50;
		  posx = 200;
		  if (j != 0)
			 row++;
	   }
	   rec.setPosition(Vector2f(posx, posy));
	   posx += 50;
	   j++;

	   if (row == 0)
		  rec.setFillColor(Color(255, 0, 0));
	   if (row == 1)
		  rec.setFillColor(Color(0, 0, 255));
	   if (row == 2)
		  rec.setFillColor(Color(0, 255, 0));
	   if (row == 3)
		  rec.setFillColor(Color(153, 0, 153));
	   if (row == 4)
		  rec.setFillColor(Color(153, 76, 0));
    }
}

///////////////////////
// Fire enemy bullet
///////////////////////
void fireEnemyBullet(void)
{
     if (startingFireDelayClock.getElapsedTime() < startingFireDelay)
     {
          //Do nothing
     }
     else if (!enemyCount == 0)
     {
          enemyBullet enemybullet;
          enemyLaser.play();
          bulletsFired.push_back(enemybullet);
     }
}

///////////////////////
// Fire bullet
///////////////////////
void fireBullet(void)
{
    if (arcadeMode == true)
    {
	   if (playerFireDelayClock.getElapsedTime() > playerFireDelay)
	   {
		  playerFireDelayClock.restart();
		  laser.play();
		  RectangleShape playerBulletRec(Vector2f(2, 10));
		  playerBullet playerbullet;
		  playerBulletsFired.push_back(playerbullet);
		  playerBulletsFiredDraw.push_back(playerBulletRec);
	   }
    }
    else
    {
	   laser.play();
	   bulletX = player.getPosition().x + 23;
	   bulletY = player.getPosition().y;
	   bullet.setPosition(bulletX, bulletY);
    }
}

//////////////////////////////////////////////
// Check Player Collision with enemy bullet
//////////////////////////////////////////////
bool checkPlayerCollision(enemyBullet b)
{
	 //Pruning
     if (checkPlayerCollisionPruningAsm(b.bulletY) == 0)
     {
          return false;
     }
     // Check Top box
     if (checkPlayerCollisionTopBoxAsm(b.originX, b.originY, playerRecA.getPosition().x, playerRecA.getPosition().y) == 1)
     {
          return true;
     }
     // Check Bottom box
     if (checkPlayerCollisionBottomBoxAsm(b.originX, b.originY, playerRecB.getPosition().x, playerRecB.getPosition().y) == 1)
     {
          return true;
     }
	 
     return false;
}

//////////////////////////////////////////////
// Check Bunker Collision with enemy bullet
//////////////////////////////////////////////
bool checkBunkerCollision(enemyBullet b)
{
     // Pruning
     /*
     if (b.bulletY < 430)
     {
          return false;
     }
     */
     if (checkBunkerCollisionPruningAsm(b.bulletY) == 0)
     {
          return false;
     }
     // First Bunker check
     if (checkBunkerCollisionBOnePruningAsm(b.bulletX) == 0)
     {
          for (int i = 0; i < 9; i++)
          {    
               if (checkBunkerCollisionBOneAsm(b.originX, b.originY, bunkerVec.at(i).getPosition().x, bunkerVec.at(i).getPosition().y) == 1)
               {
                    if (bunkerVecVal[i] == 0)
                         bunkerVec.at(i).setPosition(Vector2f(1000, 1000));
                    else
                         bunkerVecVal[i]--;
                    bunkerVec[i].setFillColor(Color(255, 96, 16, 255 - (255 / (bunkerVecVal[i] + 2))));
                    return true;
               }
               /*
               if (((b.originY + 5) >= (bunkerVec.at(i).getPosition().y)) && (((b.originX + 1) >= (bunkerVec.at(i).getPosition().x)) && ((b.originX + 1) <= (bunkerVec.at(i).getPosition().x + 22))) || ((b.originY + 5) >= (bunkerVec.at(i).getPosition().y)) && (((b.originX - 1) <= (bunkerVec.at(i).getPosition().x)) && ((b.originX - 1) >= (bunkerVec.at(i).getPosition().x + 22))))
               {
                    if (bunkerVecVal[i] == 0)
                         bunkerVec.at(i).setPosition(Vector2f(1000, 1000));
                    else
                         bunkerVecVal[i]--;
                    bunkerVec[i].setFillColor(Color(255, 96, 16, 255 - (255 / (bunkerVecVal[i] + 2))));
                    return true;
               }
               */
          }
     }
     // Second Bunker check
     else if (checkBunkerCollisionBTwoPruningAsm(b.bulletX) == 0)
     {
          for (int i = 9; i < 18; i++)
          {
               if (checkBunkerCollisionBTwoAsm(b.originX, b.originY, bunkerVec.at(i).getPosition().x, bunkerVec.at(i).getPosition().y) == 1)
               {
                    if (bunkerVecVal[i] == 0)
                         bunkerVec.at(i).setPosition(Vector2f(1000, 1000));
                    else
                         bunkerVecVal[i]--;
                    bunkerVec[i].setFillColor(Color(255, 96, 16, 255 - (255 / (bunkerVecVal[i] + 2))));
                    return true;
               }

          }
     }
     // Third Bunker check
     else if (checkBunkerCollisionBThreePruningAsm(b.bulletX) == 0)
     {
          for (int i = 18; i < 27; i++)
          {
               if (checkBunkerCollisionBThreeAsm(b.originX, b.originY, bunkerVec.at(i).getPosition().x, bunkerVec.at(i).getPosition().y) == 1)
               {
                    //bunkerVec.at(i).setPosition(Vector2f(1000, 1000));
                    if (bunkerVecVal[i] == 0)
                         bunkerVec.at(i).setPosition(Vector2f(1000, 1000));
                    else
                         bunkerVecVal[i]--;
				//bunkerVec[i].setFillColor(Color(255, 96, 16, bunkerVecVal[i] * 50));
                    bunkerVec[i].setFillColor(Color(255, 96, 16, 255 - (255 / (bunkerVecVal[i] + 2))));
                    return true;
               }

          }
     }
     // Fourth Bunker check
     else if (checkBunkerCollisionBFourPruningAsm(b.bulletX) == 0)
     {
          for (int i = 27; i < 36; i++)
          {
               if (checkBunkerCollisionBFourAsm(b.originX, b.originY, bunkerVec.at(i).getPosition().x, bunkerVec.at(i).getPosition().y) == 1)
               {
                    //bunkerVec.at(i).setPosition(Vector2f(1000, 1000));
                    if (bunkerVecVal[i] == 0)
                         bunkerVec.at(i).setPosition(Vector2f(1000, 1000));
                    else
                         bunkerVecVal[i]--;
                    bunkerVec[i].setFillColor(Color(255, 96, 16, 255 - (255 / (bunkerVecVal[i] + 2))));
                    return true;
               }

          }
     }
     return false;
}

///////////////////////
// Upgrade input
///////////////////////
int checkUpgradeInput()
{
          if (Keyboard::isKeyPressed(Keyboard::O))
          {
               return 0;
          }
          else if (Keyboard::isKeyPressed(Keyboard::D))
          {
               return 1;
          }
          else
          {
               return 2;
          }

}
///////////////////////
// User keyboard input
///////////////////////
void checkPlayerInput()
{
     Vector2f playerPos = player.getPosition();
	if (dying == false)
	{
	    if (Keyboard::isKeyPressed(Keyboard::Left))
	    {
		   player.setPosition(playerPos.x - 3, playerPos.y);
		   playerRecA.setPosition((playerPos.x + 17) - 3, playerPos.y);
		   playerRecB.setPosition(playerPos.x - 3, (playerPos.y + 20));
		   if (player.getPosition().x <= 0)
		   {
			  player.setPosition(3, playerPos.y);
			  playerRecA.setPosition(20, playerPos.y);
			  playerRecB.setPosition(3, playerPos.y + 20);
		   }
	    }
	    if (Keyboard::isKeyPressed(Keyboard::Right))
	    {
		   player.setPosition(playerPos.x + 3, playerPos.y);
		   playerRecA.setPosition((playerPos.x + 17) + 3, playerPos.y);
		   playerRecB.setPosition(playerPos.x + 3, (playerPos.y + 20));
		   if (player.getPosition().x >= 750)
		   {
			  player.setPosition(750, playerPos.y);
			  playerRecA.setPosition(767, playerPos.y);
			  playerRecB.setPosition(750, playerPos.y + 20);
		   }
	    }
	    if (Keyboard::isKeyPressed(Keyboard::Space) /*&& isFired=false */)
	    {
		   if (arcadeMode == true)
		   {
			  //isFired = true;
			  fireBullet();
		   }
		   else if (isFired == false)
		   {
			  isFired = true;
			  fireBullet();
		   }
	    }
	}
	 if (Keyboard::isKeyPressed(Keyboard::T))
	 {
		testMode = !testMode;
	 }
}

///////////////////////
// Load Scores
///////////////////////
void loadScores()
{
     int line = 1;
     string read;
     string name;
     string scoreVal;
     char let = 'x';

     //load classic
     ifstream Classic("classic.txt");
     if (Classic.is_open())
     {
          while (getline(Classic, read))
          {
               if (ModFunc(line, 2) == 0)
               {
                    if (read != " ")
                    {
                         scoreVal = read;
                         highScores[stoi(scoreVal)] = name;
                    }
               }
               else
               {
                    name = read;
               }

               line += 1;
          }
     }
     else cout << "Unable to read Scores" << endl;

     //load arcade
     ifstream Arcade("arcade.txt");
     if (Arcade.is_open())
     {
          while (getline(Arcade, read))
          {
               if (ModFunc(line, 2) == 0)
               {
                    if (read != " ")
                    {
                         scoreVal = read;
                         arcadeScores[stoi(scoreVal)] = name;
                    }
               }
               else
               {
                    name = read;
               }

               line += 1;
          }
     }
     else cout << "Unable to read Scores" << endl;

}

/*

References

Background. http://feelgrafix.com/871221-planet-wallpaper.html.
	2015. Web. 21 March 2016.

Irvine, Kip. http://kipirvine.com/asm/. 2014. Web. 1 March 2016. 

Moon. http://www.wnd.com/2015/09/why-this-coming-blood-moon-is-important-to-watch/. 2015. Web. 1 March 2016.

Sprite Ship. http://millionthvector.blogspot.com/2014/06/new-free-sprite-red-spaceship.html. 2014. Web. 1 March 2016.
*/