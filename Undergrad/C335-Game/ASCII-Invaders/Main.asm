; ASM function called from C++

.586
.model flat, C

; External C++ functions:

startGameLoop PROTO



.data
	score dword 0
	lives dword 3
	enemyArray dword 50, 100
	valX dword 20
	valY dword 100
	
Fun STRUCT
    sup dw 100
    len dw 10
Fun ENDS
ff Fun {}
.code

getShipsx proc
	mov eax, valX
	ret
getShipsx endp

getShipsy proc
	mov eax, valY
	ret
getShipsy endp

UpdateScore proc
     add score, 10
	mov eax, score
	ret
UpdateScore endp

DecLives proc
	dec lives
DecLives endp

getLives proc
	mov eax, lives
	ret
getLives endp

setLives proc
     push ebp
	mov ebp, esp
	mov eax, [ebp+8]
	mov lives, eax
	pop ebp
setLives endp

CheckCol proc
push ebp
mov ebp, esp
mov eax, [ebp+8]
mov ebx, [ebp+4]
cmp eax, ebx
je yesa
jmp noa

yesa:
mov eax, 1
pop ebp
ret

noa:
mov eax, 0
pop ebp
ret
CheckCol endp

MoveX proc
mov eax, 0
add eax, 5
ret
MoveX endp

MoveY proc
mov eax, 0
add eax, 30
ret
MoveY endp

ModFunc proc
push ebp
mov ebp, esp
mov eax, [ebp+8]
mov ebx, [ebp+12]
xor edx, edx
idiv ebx
mov eax, edx
pop ebp
ret
ModFunc endp

ClearScore proc
mov score, 0
ret
ClearScore endp

CheckCollision proc ;public bool CheckCollision(int sx, sy, bx, by)
push ebp
mov ebp, esp
mov eax, [ebp+16]
mov ebx, [ebp+12]
mov ecx, [ebp+8]
mov edx, [ebp+4]

OutIf:
cmp eax, ecx
je InIf
jmp EndIfout

InIf:
cmp ebx, edx
jle truea
jmp EndIfout

truea:
mov eax, 1
pop ebp
ret

EndIfout:
mov eax, 0
pop ebp
ret
CheckCollision endp

checkPlayerCollisionPruningAsm proc 
          push ebp
          mov ebp, esp
          mov ecx, 540
     aIf:
          cmp eax, ecx
          JL aThen
          JMP aEndIf
     aThen:
          mov eax, 0
     aEndIf:
          pop ebp
          ret
checkPlayerCollisionPruningAsm endp

checkPlayerCollisionTopBoxAsm proc
               push ebp
               mov ebp, esp
               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          bIf:
               cmp eax, ecx
               JGE bThen
               JMP bEndIf
          bThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    add eax, 1
               cIf:
                    cmp eax, ecx
                    JGE cThen
                    JMP bEndIf
               cThen:
                         add ecx, 16
                    dIf:
                         cmp eax, ecx
                         JLE dThen
                         JMP bEndIf
                    dThen:
                         mov eax, 1
                         pop ebp
                         ret
          bEndIf:

               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          eIf:
               cmp eax, ecx
               JGE eThen
               JMP eEndIf
          eThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    sub eax, 1
               fIf:
                    cmp eax, ecx
                    JLE fThen
                    JMP eEndIf
               fThen:
                         add ecx, 16
                    gIf:
                         cmp eax, ecx
                         JGE gThen
                         JMP eEndIf
                    gThen:
                         mov eax, 1
                         pop ebp
                         ret
          eEndIf:
               mov eax, 0
               pop ebp
               ret
checkPlayerCollisionTopBoxAsm endp

checkPlayerCollisionBottomBoxAsm proc
               push ebp
               mov ebp, esp
               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          hIf:
               cmp eax, ecx
               JGE hThen
               JMP hEndIf
          hThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    add eax, 1
               iIf:
                    cmp eax, ecx
                    JGE iThen
                    JMP hEndIf
               iThen:
                         add ecx, 50
                    jIf:
                         cmp eax, ecx
                         JLE jThen
                         JMP hEndIf
                    jThen:
                         mov eax, 1
                         pop ebp
                         ret
          hEndIf:

               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          kIf:
               cmp eax, ecx
               JGE kThen
               JMP kEndIf
          kThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    sub eax, 1
               lIf:
                    cmp eax, ecx
                    JLE lThen
                    JMP kEndIf
               lThen:
                         add ecx, 50
                    mIf:
                         cmp eax, ecx
                         JGE mThen
                         JMP kEndIf
                    mThen:
                         mov eax, 1
                         pop ebp
                         ret
          kEndIf:
               mov eax, 0
               pop ebp
               ret
checkPlayerCollisionBottomBoxAsm endp

checkBunkerCollisionPruningAsm proc
                    push ebp
                    mov ebp, esp
                    aIf:
                         cmp eax, 430
                         JL aThen
                         JMP aEndIf
                    aThen:
                         mov eax, 0
                    aEndIf:
                    pop ebp
                    ret
checkBunkerCollisionPruningAsm endp

checkBunkerCollisionBOnePruningAsm proc
                         push ebp
                         mov ebp, esp
                    aIf:
                         cmp eax, 167
                         JLE aThen
                         JMP aEndIf
                    aThen:
                         mov eax, 0
                    aEndIf:
                         pop ebp
                         ret
checkBunkerCollisionBOnePruningAsm endp

checkBunkerCollisionBOneAsm proc
                    push ebp
               mov ebp, esp
               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          bIf:
               cmp eax, ecx
               JGE bThen
               JMP bEndIf
          bThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    add eax, 1
               cIf:
                    cmp eax, ecx
                    JGE cThen
                    JMP bEndIf
               cThen:
                         add ecx, 22
                    dIf:
                         cmp eax, ecx
                         JLE dThen
                         JMP bEndIf
                    dThen:
                         mov eax, 1
                         pop ebp
                         ret
          bEndIf:

               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          eIf:
               cmp eax, ecx
               JGE eThen
               JMP eEndIf
          eThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    sub eax, 1
               fIf:
                    cmp eax, ecx
                    JLE fThen
                    JMP eEndIf
               fThen:
                         add ecx, 22
                    gIf:
                         cmp eax, ecx
                         JGE gThen
                         JMP eEndIf
                    gThen:
                         mov eax, 1
                         pop ebp
                         ret
          eEndIf:
               mov eax, 0
               pop ebp
               ret
checkBunkerCollisionBOneAsm endp

checkBunkerCollisionBTwoPruningAsm proc
                         push ebp
                         mov ebp, esp
                    aIf:
                         cmp eax, 342
                         JLE aThen
                         JMP aEndIf
                    aThen:
                         bIf:
                              cmp eax, 274
                              JGE bThen
                              JMP aEndIf
                         bThen:
                              mov eax, 0
                    aEndIf:
                         pop ebp
                         ret
checkBunkerCollisionBTwoPruningAsm endp

checkBunkerCollisionBTwoAsm proc
                    push ebp
               mov ebp, esp
               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          bIf:
               cmp eax, ecx
               JGE bThen
               JMP bEndIf
          bThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    add eax, 1
               cIf:
                    cmp eax, ecx
                    JGE cThen
                    JMP bEndIf
               cThen:
                         add ecx, 22
                    dIf:
                         cmp eax, ecx
                         JLE dThen
                         JMP bEndIf
                    dThen:
                         mov eax, 1
                         pop ebp
                         ret
          bEndIf:

               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          eIf:
               cmp eax, ecx
               JGE eThen
               JMP eEndIf
          eThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    sub eax, 1
               fIf:
                    cmp eax, ecx
                    JLE fThen
                    JMP eEndIf
               fThen:
                         add ecx, 22
                    gIf:
                         cmp eax, ecx
                         JGE gThen
                         JMP eEndIf
                    gThen:
                         mov eax, 1
                         pop ebp
                         ret
          eEndIf:
               mov eax, 0
               pop ebp
               ret
checkBunkerCollisionBTwoAsm endp

checkBunkerCollisionBThreePruningAsm proc
                         push ebp
                         mov ebp, esp
                    aIf:
                         cmp eax, 517
                         JLE aThen
                         JMP aEndIf
                    aThen:
                         bIf:
                              cmp eax, 449
                              JGE bThen
                              JMP aEndIf
                         bThen:
                              mov eax, 0
                    aEndIf:
                         pop ebp
                         ret
checkBunkerCollisionBThreePruningAsm endp

checkBunkerCollisionBThreeAsm proc
                    push ebp
               mov ebp, esp
               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          bIf:
               cmp eax, ecx
               JGE bThen
               JMP bEndIf
          bThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    add eax, 1
               cIf:
                    cmp eax, ecx
                    JGE cThen
                    JMP bEndIf
               cThen:
                         add ecx, 22
                    dIf:
                         cmp eax, ecx
                         JLE dThen
                         JMP bEndIf
                    dThen:
                         mov eax, 1
                         pop ebp
                         ret
          bEndIf:

               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          eIf:
               cmp eax, ecx
               JGE eThen
               JMP eEndIf
          eThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    sub eax, 1
               fIf:
                    cmp eax, ecx
                    JLE fThen
                    JMP eEndIf
               fThen:
                         add ecx, 22
                    gIf:
                         cmp eax, ecx
                         JGE gThen
                         JMP eEndIf
                    gThen:
                         mov eax, 1
                         pop ebp
                         ret
          eEndIf:
               mov eax, 0
               pop ebp
               ret
checkBunkerCollisionBThreeAsm endp

checkBunkerCollisionBFourPruningAsm proc
                         push ebp
                         mov ebp, esp
                    aIf:
                         cmp eax, 692
                         JLE aThen
                         JMP aEndIf
                    aThen:
                         bIf:
                              cmp eax, 624
                              JGE bThen
                              JMP aEndIf
                         bThen:
                              mov eax, 0
                    aEndIf : 
                         pop ebp
                         ret
checkBunkerCollisionBFourPruningAsm endp

checkBunkerCollisionBFourAsm proc
                    push ebp
               mov ebp, esp
               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          bIf:
               cmp eax, ecx
               JGE bThen
               JMP bEndIf
          bThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    add eax, 1
               cIf:
                    cmp eax, ecx
                    JGE cThen
                    JMP bEndIf
               cThen:
                         add ecx, 22
                    dIf:
                         cmp eax, ecx
                         JLE dThen
                         JMP bEndIf
                    dThen:
                         mov eax, 1
                         pop ebp
                         ret
          bEndIf:

               mov eax, [ebp + 12] ;OriginY
               mov ecx, [ebp + 20] ;PlayerRecA.y
               add eax, 5
          eIf:
               cmp eax, ecx
               JGE eThen
               JMP eEndIf
          eThen:
                    mov eax, [ebp + 8]
                    mov ecx, [ebp + 16]
                    sub eax, 1
               fIf:
                    cmp eax, ecx
                    JLE fThen
                    JMP eEndIf
               fThen:
                         add ecx, 22
                    gIf:
                         cmp eax, ecx
                         JGE gThen
                         JMP eEndIf
                    gThen:
                         mov eax, 1
                         pop ebp
                         ret
          eEndIf:
               mov eax, 0
               pop ebp
               ret
checkBunkerCollisionBFourAsm endp

; ----------------------------------------------
asmMain PROC
; Main entry point to asm program.
; ----------------------------------------------
     INVOKE startGameLoop
     ret
asmMain ENDP

END