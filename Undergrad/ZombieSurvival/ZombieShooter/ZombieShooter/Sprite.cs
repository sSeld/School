﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZombieShooter
{
     public abstract class Sprite
     {
          protected Texture2D texture;
          protected Vector2 Location;
          protected Rectangle boundaries;

          public Vector2 Velocity { get; protected set; }

          protected Sprite(Vector2 location, Rectangle boundaries)
          {
               this.Location = location;
               this.boundaries = boundaries;
          }
          protected Sprite(Texture2D texture, Vector2 location, Rectangle boundaries)
          {
               this.Location = location;
               this.boundaries = boundaries;
               this.texture = texture;
          }

          public virtual void Draw(SpriteBatch spriteBatch)
          {
               spriteBatch.Draw(texture, Location, Color.White);
          }

          public virtual void Update(GameTime gameTime)
          {
               Location += Velocity;
               CheckBounds();
          }

          protected abstract void CheckBounds();
     }
}