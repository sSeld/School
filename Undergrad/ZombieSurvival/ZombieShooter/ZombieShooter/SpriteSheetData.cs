﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace ZombieShooter
{
     public class SpriteSheetData
     {
          ///////////////////////
          // Player
          ///////////////////////
          /// Rifle

          public Rectangle[] rifleMove =
          {
               new Rectangle(0, 0, 313, 206),
               new Rectangle(0, 207, 313, 206),
               new Rectangle(314, 0, 313, 206),
               new Rectangle(314, 414, 313, 206),
               new Rectangle(314, 621, 313, 206),
               new Rectangle(628, 207, 313, 206),
               new Rectangle(628, 621, 313, 206),
               new Rectangle(628, 828, 313, 206),
               new Rectangle(314, 1035, 313, 206),
               new Rectangle(628, 1035, 313, 206),
               new Rectangle(942, 207, 313, 206),
               new Rectangle(942, 0, 313, 206),
               new Rectangle(0, 1035, 313, 206),
               new Rectangle(314, 828, 313, 206),
               new Rectangle(0, 828, 313, 206),
               new Rectangle(628, 414, 313, 206),
               new Rectangle(628, 0, 313, 206),
               new Rectangle(0, 621, 313, 206),
               new Rectangle(314, 207, 313, 206),
               new Rectangle(0, 414, 313, 206)};

          public Rectangle[] feetWalk =
          {
               new Rectangle(628,1035,313,206),
               new Rectangle(314,1035,313,206),
               new Rectangle(628,828,313,206),
               new Rectangle(0,828,313,206),
               new Rectangle(628,414,313,206),
               new Rectangle(628,0,313,206),
               new Rectangle(0,621,313,206),
               new Rectangle(314,207,313,206),
               new Rectangle(942,0,313,206),
               new Rectangle(0,207,313,206),
               new Rectangle(942,207,313,206),
               new Rectangle(0,0,313,206),
               new Rectangle(0,414,313,206),
               new Rectangle(314,0,313,206),
               new Rectangle(314,414,313,206),
               new Rectangle(314,621,313,206),
               new Rectangle(628,207,313,206),
               new Rectangle(628,621,313,206),
               new Rectangle(314,828,313,206),
               new Rectangle(0,1035,313,206)
          };

          public Rectangle[] feetRun =
          {
               new Rectangle(0, 0, 313, 206),
               new Rectangle(0, 414, 313, 206),
               new Rectangle(314, 0, 313, 206),
               new Rectangle(314, 414, 313, 206),
               new Rectangle(628, 0, 313, 206),
               new Rectangle(628, 207, 313, 206),
               new Rectangle(628, 621, 313, 206),
               new Rectangle(628, 828, 313, 206),
               new Rectangle(0, 1035, 313, 206),
               new Rectangle(942, 0, 313, 206),
               new Rectangle(942, 207, 313, 206),
               new Rectangle(628, 1035, 313, 206),
               new Rectangle(314, 1035, 313, 206),
               new Rectangle(314, 828, 313, 206),
               new Rectangle(0, 828, 313, 206),
               new Rectangle(628, 414, 313, 206),
               new Rectangle(314, 621, 313, 206),
               new Rectangle(0, 621, 313, 206),
               new Rectangle(314, 207, 313, 206),
               new Rectangle(0, 207, 313, 206)
          };
     }
}

