﻿using System;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ZombieShooter
{
     public class Player : Sprite
     {
          private const float PLAYER_WALK = 3.0f;
          private const float PLAYER_RUN = 6.0f;
          private float PLAYER_SPEED = PLAYER_WALK;
          private readonly SpriteSheetData _spriteFrames = new SpriteSheetData();
          private int _frameNum = 0;
          private int timeLastFrame = 0;
          private int milsecPerFrame = 50;
          private readonly Texture2D _rifleMove;
          private readonly Texture2D _feetWalk;
          private readonly Texture2D _feetRun;
          private bool IsRunning;

          public Player(Texture2D feetRun, Texture2D feetWalk, Texture2D rifleMove, Vector2 location, Rectangle screenBoundaries) : base(location, screenBoundaries)
          {
               this._feetRun = feetRun;
               this._feetWalk = feetWalk;
               this._rifleMove = rifleMove;
          }

          public override void Update(GameTime gameTime)
          {
               timeLastFrame += gameTime.ElapsedGameTime.Milliseconds;
               //////////////////////////////
               // Check player input
               //////////////////////////////
               //// Check for run
               if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
               {
                    IsRunning = true;
                    PLAYER_SPEED = PLAYER_RUN;
                    milsecPerFrame = 25;
               }
               else
               {
                    IsRunning = false;
                    PLAYER_SPEED = PLAYER_WALK;
                    milsecPerFrame = 50;
               }

               //////////////////////////////
               //// Check which key is pressed
               if (Keyboard.GetState().IsKeyDown(Keys.D))
               {
                    Velocity = new Vector2(PLAYER_SPEED, 0);
                    CheckFrames();
                    base.Update(gameTime);
               }
               if (Keyboard.GetState().IsKeyDown(Keys.A))
               {
                    Velocity = new Vector2(-PLAYER_SPEED, 0);
                    CheckFrames();
                    base.Update(gameTime);
               }
               if (Keyboard.GetState().IsKeyDown(Keys.W))
               {
                    Velocity = new Vector2(0, -PLAYER_SPEED);
                    CheckFrames();
                    base.Update(gameTime);
               }
               if (Keyboard.GetState().IsKeyDown(Keys.S))
               {
                    Velocity = new Vector2(0, PLAYER_SPEED);
                    CheckFrames();
                    base.Update(gameTime);
               }
          }

          public override void Draw(SpriteBatch spriteBatch)
          {
               if (IsRunning)
                    spriteBatch.Draw(_feetRun, Location, _spriteFrames.feetRun[_frameNum], Color.White);
               else
                    spriteBatch.Draw(_feetWalk, Location, _spriteFrames.feetWalk[_frameNum], Color.White);
                    

               spriteBatch.Draw(_rifleMove, Location, _spriteFrames.rifleMove[_frameNum], Color.White);
          }

          protected override void CheckBounds()
          {
               Location.Y = MathHelper.Clamp(Location.Y, 0, boundaries.Height - _spriteFrames.rifleMove[_frameNum].Height);
               Location.X = MathHelper.Clamp(Location.X, 0, boundaries.Width - _spriteFrames.rifleMove[_frameNum].Width);
          }

          private void CheckFrames()
          {
               if (timeLastFrame > milsecPerFrame)
               {
                    timeLastFrame -= milsecPerFrame;
                    _frameNum++;
                    timeLastFrame = 0;
               }
               if (_frameNum > 19)
                    _frameNum = 0;
          }
     }
}