﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ZombieShooter
{
     public class SpriteSheet
     {
          private Texture2D sheet;
          private Rectangle[] frames;

          public SpriteSheet(Texture2D sheet, Rectangle[] frames)
          {
               this.sheet = sheet;
               this.frames = frames;
          }

     }
}
