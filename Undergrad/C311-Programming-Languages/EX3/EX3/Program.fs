﻿// Jeremy Stephens
// C311 Programming Languages
// 09/14/2016
// Exercise 3

// #1
// Write a function named dist to construct a list of tuples from one element paired with every element of a list.
let rec dist x L =
    match L with
    | [] -> []
    | h::t -> (x, h)::(dist x t);;

dist 3 ['a';'b';'c'];;

// #2
// Write a function named pairs that constucts all possible tuples of two lists.


let rec pairs L1 L2 =
    match L1 with
    | [] -> []
    | h::[] -> [dist h L2]
    | h::t -> dist h L2::pairs t L2;;

pairs [1;2;3;4] ["a";"b";"c";"d"];;

// #3 & 4
// Define appropriate F# types for the following:
// enumerate the set coin using names of US coins worth $.100 or less.
// the union of SIR: a string, int, or real.
// a type that combines coin and SIR to define the union SIRC
// Define a function, amount that totals the decimal amount of a list of coin. For example:

type coins = 
    | Nickel = 5
    | Dime = 10
    | Dollar = 100
    | Quarter = 25;; 

type SIR = 
    | S of string
    | I of int
    | F of float;;

type SIRC = 
    | C of coins
    | SIR of SIR;;

let rec amount L =
    match L with
    | [] -> 0
    | h::t -> h + amount t;;

amount [Dollar; Dollar; Dime; Nickel];;