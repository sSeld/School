﻿// Jeremy Stephens
// C311 Programming Languages
// 9/12/2016
// Homework 2

// #1
// Define function to add 2-tuples. Hint: use pattern matching.
let add (x, y) (a, b) = (x+a, y+b);;

add (3,4) (5,6);;

// #2
// Turn any list into a tuple of 3 lists
// O(n) n would equal the list size / 3
let rec third L =
    match L with
    | [] -> ([], [], [])
    | h::[] -> ([h], [], [])
    | a::b::c::ds -> 
        let (x, y, z) = third(ds) in (a::x, b::y, c::z);;

third [1;2;3;1;2;3;1;2;3;1;2;3];;

// #3
// Define the functional dup so that dup (f, x) is f (f x) where f parameter is a type and f returns that type.
let inc (x: int) : int = x + 1;;
let dup f x = f (f x);;
dup inc 5;;

let tail (L : 'a list) = L.Tail;;
dup tail [1;2;3;4;5];;

// #4
// Define the functional comp so that comp(f,g,x) is g(f (x)) where f and g receive an Int and return an Int.
let comp f g (x: int) : int = g (f x);;

comp inc inc 5;;

// #5
// Define the functional comp so that comp(f,g,x) is g(f (x)) where f parameter and result are same type, and g parameter is result type of f.
let above10 x = x > 10;;
let comp2 f g (x: int) = g (f x);;

comp2 inc inc 5;;
comp2 inc above10 10;;

// #6
// Define a function sqlist to square every element of a given list using sq and the map functional where let sq x = x * x;;
// O(n) with n being the size of the list
// Because it will recurse through every element of the list squaring
let sq x = x*x;;
let rec sqlist (L: int list) =
    match L with
    | [] -> []
    | h::[] -> [sq h]
    | h::t -> sq h::sqlist t;;

sqlist [1;2;3;4];;

// #7
// Define a function vecadd to add two integer lists using map2 and add where let add x y = x + y;;
let vecadd x y = x + y;;
let rec map2 f L1 L2 = 
  match (L1,L2) with
    | ([], []) -> []
    | (h1::t1, h2::t2) -> f h1 h2::map2 f t1 t2;;

map2 vecadd [1;2;3] [4;5;6];;

// #8
// Define a function matadd to add two integer matrices using vecadd for vector addition and map2 for recursion. matadd is a one line function.
let rec matadd f L1 L2 =
 match (L1, L2) with
  | ([], []) -> []
  | (h1::t1, h2::t2) -> f h1 h2::matadd f t1 t2;;

matadd (map2 vecadd) [[1;2];[3;4]][[5;6];[7;8]];;

// #9
// Define a function ip to compute the inner product of two lists using map2 and reduce. The inner product is the sum of the products of multiplying two lists. In the class notes we defined a function such that reduce add 0 [1;2;3] = 6 and map2 mul [1;2;3] [4;5;6] = [4;10;18]. ip is a one line function.
let mult a b = a * b;;
let rec reduce f a L =
  match L with
    | [] -> a
    | h::t -> f h (reduce f a t);;

let rec ip L1 L2 : int =
    match L1, L2 with
    | [],[] -> 0
    | h1::t1, h2::t2 -> mult h1 h2 + ip t1 t2;;

ip [1;2;3] [1;2;3];;