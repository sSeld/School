﻿// Jeremy Stephens
// C311 Programming Languages
// 09/23/16
// Homework 4


// #1
// Give a tail recursive reverse function of O(n) for the definition of mylist:
type 'element mylist = NIL | CONS of 'element * 'element mylist;;

let rec helper a b : int mylist =
  match a with
    | NIL -> b
    | CONS (h, t) -> helper t (CONS (h, b));;

let rev L = helper L NIL;;
let c = CONS (1, CONS (2, CONS (3, NIL)));;
rev c;;    // returns int mylist = CONS (3, CONS (2, CONS (1, NIL)))

// #2
// This version of vecadd must calculate the recursive values of vecadd before consing onto (h1+h2). A recrusive version would cons onto a known output value for each recursion step passing down by an argument and pass the final output back up in the base case. This would have a different signature than vecadd, and so you should write a helper function. Give a tail recursive version of vecadd function.

let rec vecadd v1 v2 =
  match (v1, v2) with
    | ([], []) -> []
    | (h1::t1, h2::t2) -> vahelper t1 t2 (h1+h2);;
//vecadd (h1+h2) (vecadd v1 v2);;

let rec vahelper v1 v2 out = vecadd v1 v2::out::[];;

vecadd [1;2;3] [4;5;6];;

// a. O(n)
// b. 

// #5
// Modify mergeSort to use a function parameter of (int * int) -> bool in place of x < y to determine whether to sort ascending or descending. For example:

let rec mergeSort (f:int -> int -> bool) L =
 match L with
  | [] -> []
  | _::[] -> L
  | theList ->
   let rec halve L =
    match L with
     | [] -> ([], [])
     | a::[] -> ([a], [])
     | a::b::cs ->
       let (x, y) = halve cs
       (a::x, b::y)
   let rec merge (L1, L2) =
    match (L1, L2) with
     | ([], ys) -> ys
     | (xs, []) -> xs
     | (x::xs, y::ys) when f x y -> x::merge(xs,y::ys)
     | (x::xs, y::ys) -> y::merge(x::xs,ys)
   let (x, y) = halve theList
   in
   merge (mergeSort f x, mergeSort f y);;

mergeSort (fun x y -> x<y) [4;2;6;5];;          // returns int list = [2;4;5;6]
mergeSort (fun x y -> x>y) [4;2;6;5];;         // returns int list = [6;5;4;2]

//#6
// Curry mergeSort of a. Test by:
// Modify mergeSort to sort a mylist of any type on which relations > and < are defined. Test by:

mergeSort (fun x y -> x<y) [1;6;2;7];;        // returns int list = [1;2;6;7]
let sortAscending = mergeSort (fun x y -> x<y);;  // returns int list -> int list -> <function>
sortAscending [1;6;2;7];;        // returns int list = [1;2;6;7]
let sortDescending = mergeSort (fun x y -> x>y)  // returns int list -> int list -> <function>
sortDescending [1;6;2;7];;        // returns int list = [7;6;2;1]

let rec mergeSort2 (f:int -> int -> bool) L  =
 match L with
  | NIL -> NIL
  | CONS (_, NIL) -> L
  | theList ->
   let rec halve L =
    match L with
     | NIL -> (NIL, NIL)
     | CONS (a, NIL) -> (CONS (a, NIL), NIL)
     | CONS (a, CONS(b, cs)) ->
       let (x, y) = halve cs
       (CONS (a, x), CONS (b,y))
   let rec merge (L1, L2) =
    match (L1, L2) with
     | (NIL, ys) -> ys
     | (xs, NIL) -> xs
     | (CONS (x, xs), CONS (y, ys)) when f x y -> CONS (x, merge(xs, CONS (y, ys)))
     | (CONS (x, xs), CONS (y, ys)) -> CONS (y, merge(CONS (x, xs), ys))
   let (x, y) = halve theList
   in
   merge (mergeSort2 f x, mergeSort2 f y);;

mergeSort2 (fun x y -> x>y) (CONS (6, CONS (12, CONS (3, NIL))));; // returns int mylist = CONS(12,CONS(6,CONS(3,NIL)))
