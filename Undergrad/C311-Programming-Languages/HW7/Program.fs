﻿// Jeremy Stephens
// 11/02/16
// C311 Programming Languages
// Homework 7

type AST =
  | Const of int
  | Plus of AST * AST
  | Times of AST * AST
  | Var of string
  | Let of string * AST * AST
  | Fn of string * AST
  | Apply of AST * AST
  | Bool of bool
  | Sub of AST * AST
  | If of AST * AST * AST
  | Eq of AST * AST
  | Or of AST * AST;;

let rec lookup V L =
 let (var,value)::T = L in
  if V=var then value else lookup V T;;

let rec val4 exp ctx = match exp with
  | Const x -> Const x
  | Var V -> lookup V ctx
  | Plus (Const x, Const y) -> Const (x+y)
  | Plus (x, y) -> val4 (Plus(val4 x ctx, val4 y ctx)) ctx
  | Times (Const x, Const y) -> Const (x*y)
  | Times (x, y) -> val4 (Times(val4 x ctx, val4 y ctx)) ctx
  | Let (V, exp1, exp2) -> val4 exp2 ((V, val4 exp1 ctx)::ctx)
  | Fn (formal, body) -> Fn(formal, body)
  | Apply (Fn(formal, body), actual) -> val4 body ((formal, val4 actual ctx)::ctx)
  | Apply (Var v, actual) -> val4 (Apply (val4 (Var v) ctx, actual)) ctx
  | Sub (Const x, Const y) -> Const (x-y)
  | Sub (x, y) -> val4 (Sub(val4 x ctx, val4 y ctx)) ctx
  | Eq (Const x, Const y) -> Bool(x = y)
  | Eq (Bool x, Bool y) -> Bool(x=y)
  | Eq (x, y) -> val4 (Eq (val4 x ctx, val4 y ctx)) ctx
  | Or (Bool x, Bool y) -> Bool (x||y)
  | Or (x, y) -> val4 (Or (val4 x ctx, val4 y ctx)) ctx
  | If (Bool x, y, z) ->  if x then val4 y ctx else val4 z ctx
  | If (x, y, z) -> val4 (If (val4 x ctx, y, z)) ctx;;

  /////////////////////////////////

val4(Sub(Const (5),Const (1))) [];;                     // returns Const(4)
val4(Sub(Var ("n"),Const (1))) [("n",Const (5))];;      // returns Const(4)

val4(Eq(Const (3),Const (4))) [];;                                // returns Bool (false)
val4(Eq(Bool (true), Bool (false))) [];;                          // returns Bool (false)
val4(Eq(Var ("X"),Var ("Y"))) [("Y",Const (4));("X",Const (4))];; // returns Bool (true)

val4(If(Bool (true), Const (3), Const (4))) [];;                    // return Const (3)
val4(If(Or(Bool (true), Bool (false)), Const (3), Const (4))) [];;  // return Const (3)

val4(If(Eq(Const (3),Const (4)),Const (1),Const (2))) [];;                                  // returns Const 2
val4(If(Eq(Var ("A"),Var ("B")),Var ("A"),Var ("B"))) [("A",Const (4));("B",Const (3))];;   // returns Const 3
val4(If(Eq(Var ("A"),Var ("B")),Var ("A"),Var ("B"))) [("A",Const (4));("B",Bool (3))];;    // Fails  
val4(If(Eq(Var ("A"),Var ("B")),Var ("A"),Var ("B"))) [("A",Const (4));("B",Bool (true))];; // Fails


// Factorial 5, Fn is defining the function as n then If it is 0 or 1. If 0 return 1, else multiply fact by n -1
val4(
 Let( "fact",
      Fn( "n",
          If( Or(Eq(Var ("n"), Const (0)),Eq(Var ("n"), Const (1))),
              Const (1),
              Times(Var ("n"), Apply(Var ("fact"), Sub(Var ("n"),Const (1)))))),
      Apply(Var ("fact"), Const (5))))
 [];;


 // Fibonacci 6, Fn is defining the fuction as n then If it is 0 return 0, else check If n is 1 then return 1 else add fib n-1 with fib n-2.
val4(
 Let( "fib",
      Fn( "n",
          If(Eq (Var "n", Const 0),
              Const (0),
              (If(Eq(Var "n", Const 1), Const (1), Plus( Apply(Var ("fib"), Sub(Var ("n"),Const (1))),Apply(Var ("fib"), Sub(Var ("n"),Const (2)))))))),
      Apply(Var ("fib"), Const (6))))
 [];;
 
 