﻿// Learn more about F# at http://fsharp.org

// #1 Function and call
let rec GCD (x, y) = 
    if y = 0 then x else GCD(y, x%y);;

GCD(9, 18);;

// #2 Function and call
let rec factorial (n) = 
    match n with
        | 0 -> 1
        | _ -> n * factorial(n-1);;

factorial(5);;

let rec C (n,k) = 
 match (n,k) with
  | (0,k) -> 0
  | (n,0) -> 0
  | (n,k) -> factorial(n) / (factorial(k) * factorial(n - k));;

C(20, 5);;

// #3 Function
// O(n)
let rec biggest L =
 match L with
  | h :: [] -> h
  | h :: t when h > biggest t -> h
  | h :: t -> biggest t;;

biggest[2;34;2;100;22;89];;

// #4 Function
// O(n)
let rec positive L =
 match L with
  | [] -> []
  | h :: t when h < 0 -> positive t
  | h :: t when h >= 0 -> h::positive t
  | h :: t -> L;;

  positive[-5;10;23;-6;21];;

// #5 Function
// O(n2)
 let rec isMember a L =
  match L with
   | [] -> false
   | h::t when a=h -> true 
   | h::t -> isMember a t;;
isMember 2 [3;5;2;64];;

let rec intersection L1 L2 =
 match L1 with
  | [] -> []
  | h::t when isMember h L2 = true -> h::intersection t L2
  | h::t when isMember h L2 = false -> intersection t L2
  | h::t -> L1;;

intersection [5;3;1] [3;2;1];;

// #6 Function
// O(n2)
let rec insert a L =
 match L with
  | [] -> a::[]
  | h::t when a <= h -> h::insert a t
  | h::t when a >= h -> a::h::t
  | h::t -> L;;

insert 5 [10;6;3;1];;

// #7 Function
// O(n2)
let rec sort L =
 match L with
  | [] -> []
  | h::t -> insert h (sort t);;

sort [5;1;7;3;2];;

// #8 Function
let vecadd x y = x + y;;

let rec map2 f L1 L2 =
 match (L1,L2) with
  | ([], []) -> []
  | (h1::t1, h2::t2) -> f h1 h2::map2 f t1 t2;;

map2 vecadd [1;2;3][4;5;6];;


// #11 Function


let rec add f L1 L2 =
 match (L1, L2) with
  | ([], []) -> []
  | (h1::t1, h2::t2) -> f h1 h2::add f t1 t2;;

add (map2 vecadd) [[1;2];[3;4]][[5;6];[7;8]];;