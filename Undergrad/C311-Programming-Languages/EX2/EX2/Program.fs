﻿///////////////////////////
// #1
///////////////////////////
let f (x,y) = 
 match (x,y) with
  | (x, 0) -> x
  | (0, y) -> y
  | (x, y) -> x/y;;

f(0,5);;

///////////////////////////
// #2
///////////////////////////
let f L =
    if L = [] then []
    else if L.Tail = [] then L
    else L.Tail.Tail;;

f[1;2;3;6;2;5];;

let f L =
    match L with
        | [] -> []
        | L.Tail = [] -> L;;

///////////////////////////
// #3
///////////////////////////
let rec snoc a (L : 'a list) =
 if L.IsEmpty then [a]
 else L.Head::snoc a L.Tail;;
snoc 1 [1;5;2;3];;

///////////////////////////
// #4
///////////////////////////
let rec rac s =
 match s with
  | h::[] -> s.Head
  | h::t -> rac s.Tail;;
rac [1;2;3;4];;

///////////////////////////
// #5
///////////////////////////
 let rec rdc L =
  match L with
   | h::[] -> []
   | h::t -> L.Head::rdc L.Tail;;
rdc[2;3;4;5];;

///////////////////////////
// #6
///////////////////////////


let rec map f L =
 match L with
  | [] -> []
  | h::t -> (f h)::map f t;;
map sqrt [81.0; 64.0; 49.0; 36.0];;

///////////////////////////
// #7
///////////////////////////
snoc 3 [1; 2];;

let rec reduce f a L =
 match L with
  | [] -> a
  | h::t -> f h (reduce f a t);;

reduce snoc [] [1;2;3];;

///////////////////////////
// #8
///////////////////////////

let oneOver x = 1.0/x;;

let rec map f L =
 match L with
  | [] -> []
  | h::t -> (f h)::map f t;;


let neZero x =x <> 0.0;;
neZero 3.0;;
neZero 0.0;;

let rec filter f L =
 match L with
  | [] -> []
  | h::t when f h -> h::filter f t;
  | h::t -> filter f t;;

map oneOver (filter neZero [1.0; 9.0; 0.0; 8.0; 19.0]);;