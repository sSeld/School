﻿// Jeremy Stephens
// Programming Languages
// 09/19/16
// Homework 3

// #1
// You will need to define a pairs function that generates all possible pairs as tuples of elements from two lists. The signature should be:
type SET = 
   | I of int list                                    // I [1;2;3]
   | S of string list                               // S ["a";"b";"c"]
   | IS of (int * string) list                      // IS [(1, "a");(2, "b")]
   | II of (int * int) list                         // II [(1,2); (3,4); (5,6)]
   | SS of (string * string) list                   // SS [("a","b"); ("c","d")]
   | SI of (string * int) list                      // SI [("a", 1); ("b", 2); ("c", 3)]
   | SISI of ((string * int) * (string * int)) list // SISI [(("a", 1), ("b", 2)); (("c", 3), "d", 4))]
   | SIIS of ((string * int) * (int * string)) list;; // SIIS [(("a", 1), (2, "b")); (("c", 3), (4, "d"))]

let rec dist x L =
    match L with
    | [] -> []
    | h::t -> (x, h)::(dist x t);;

let rec pairs L1 L2 =
    match L1 with
    | [] -> []
    | h::t -> dist h L2@pairs t L2;;

let product s1 s2 =
  match (s1, s2) with
    | (I s1, I s2) -> II (pairs s1 s2)
    | (S s1, S s2) -> SS (pairs s1 s2)
    | (I s1, S s2) -> IS (pairs s1 s2)
    | (S s1, I i1) -> SI (pairs s1 i1)
    | (SI si1, IS is) -> SIIS (pairs si1 is)
    | (SI si1, SI si2) -> SISI (pairs si1 si2);;

let i2 = I [5555; 6666];;
let s2 = S ["CHEM"; "MATH"];;
product i2 s2;; // IxS
product s2 i2;  // SxI

let i1 = I [1111;2222;3333];;
let i2 = I [5555; 6666];;
let s1 = S ["CSCI"; "ENG"; "PHY"];;
let s2 = S ["CHEM"; "MATH"];;

let is = product i1 s1;;
let si1 = product s1 i1;;
let si2 = product s2 i2;;
let sisi = product si1 si2;;
let siis1 = product si1 is;;
let siis2 = product si2 is;; 

// #2
// Using isMember and union functions for lists from earlier homeworks and notes, implement the union operation on the SET data types SI and SIIS. The following union handles three cases and uses the old list union function renamed to unionList.

let rec isMember a L =
  match L with
    | [] -> false
    | h::t when a=h -> true
    | h::t -> isMember a t;;

let rec unionList l1 l2 =
  match l1 with
    | [] -> l2
    | h::t when not (isMember h l2) -> h::unionList t l2
    | h::t -> unionList t l2;;

let union s1 s2 =
  match (s1, s2) with
    | (I l1, I l2) -> I (unionList l1 l2)
    | (IS l1, IS l2) -> IS (unionList l1 l2)
    | (SISI l1, SISI l2) -> SISI (unionList l1 l2)
    | (SI l1, SI l2) -> SI (unionList l1 l2) 
    | (SIIS l1, SIIS l2) -> SIIS (unionList l1 l2);;

union si1 si2;;
union siis1 siis2;;

// #3
// 

let gtIx x = fun (a, _) -> a > x;;

gtIx 4 (5, "red");; // returns true

let gtxxxI x = fun ((_, _), (_, a)) -> a > x;;

gtxxxI 4 ((6,"blue"),("red",5));; // returns true

let gtSx x (a, _) = a > x;;

gtSx "red" ("blue", 5);; // returns false

gtSx "blue" ("red", 5);; // returs true

let eqIxx x ((_, a), (_, _)) = a = x;;

eqIxx 4 (("red", 4), (6, "blue"));; // returns true

eqIxx 4 (("red", 3), (6, "blue"));; // returns false

// #4
let rec filter f L =
  match L with
    | [] -> []
    | h::t when f h -> h::filter f t
    | h::t -> filter f t;;

let eqxIxx x = fun ((_, a), (_, _)) -> a = x;;

let (SIIS x) = siis2;;     // x is the list of the SIIS set
filter (eqxIxx 5555) x;; // filter 5555 entries


// #5
let selectIS s f =
  match s with
    | IS i -> IS (filter f i);;

let selectSISI s f =
  match s with
    | SISI i -> SISI (filter f i);;

let selectSI s f =
  match s with
    | SI i -> SI (filter f i);;

let selectSIIS s f =
  match s with
    | SIIS i -> SIIS (filter f i);;

selectIS is (gtIx 2222);;

selectSISI sisi (gtxxxI 2222);;

selectSI si1 (gtSx "CSCI");;
selectSIIS siis1 (eqxIxx 2222);;

// #6
//

selectSI si1 (fun (a, _) -> a > "CSCI");;    
selectSIIS siis1 (fun ((_, a), (_, _)) -> a = 2222);;

// #7

let rec remove L1 L2 =
    match L1 with 
    | [] -> []
    | h::t -> if not (isMember h L2) then h::remove t L2 else remove t L2;;

let rec difference L1 L2 =
    match L1, L2 with
    | (SI L1, SI L2) -> SI (remove L1 L2)
    | (SIIS L1, SIIS L2) -> SIIS (remove L1 L2);;

difference si1 si2;;
difference siis1 siis2;;
